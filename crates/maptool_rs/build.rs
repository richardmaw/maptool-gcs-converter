use std::{
    env,
    fs::{self, File},
    io::BufReader,
    path::{Path, PathBuf},
    process::Command,
};

use flate2::bufread::GzDecoder;
use tar::Archive;

fn main() {
    let out_dir = PathBuf::from(
        env::var("OUT_DIR").expect("build.rs should be passed OUT_DIR"),
    );
    let mut proto_dir = out_dir.clone();
    proto_dir.push("proto");
    fs::create_dir_all(&proto_dir).expect("Should be able to make proto dir");
    let inputs: Vec<Box<Path>> = fs::read_dir("maptool/src/main/proto")
        .expect("Should be able to read protobuf dir")
        .map(|entry| {
            entry
                .expect("Should be able to read proto dentries")
                .path()
                .into()
        })
        .collect();
    // TODO: make these paths relative to this file somehow?
    protobuf_codegen_pure::Codegen::new()
        .out_dir(&proto_dir)
        .inputs(&inputs)
        .include("maptool/src/main/proto")
        .customize(protobuf_codegen_pure::Customize {
            gen_mod_rs: Some(true),
            carllerche_bytes_for_bytes: Some(true),
            carllerche_bytes_for_string: Some(true),
            ..Default::default()
        })
        .run()
        .expect("Codegen should succeed");

    let cargo = env::var_os("CARGO").expect("build.rs should be passed CARGO");
    let ret = Command::new(cargo)
        .args(["package", "--no-verify", "--allow-dirty", "--target-dir"])
        .arg(&out_dir)
        .status()
        .expect("cargo package should succeed");
    assert!(ret.success());
    let pkg_name = env::var("CARGO_PKG_NAME")
        .expect("build.rs should be passed CARGO_PKG_NAME");
    let pkg_version = env::var("CARGO_PKG_VERSION")
        .expect("build.rs should be passed CARGO_PKG_VERSION");

    let crate_name = format!("{}-{}", &pkg_name, pkg_version);
    let mut crate_path = out_dir.clone();
    crate_path.push("package");
    crate_path.push(format!("{}.crate", &crate_name));
    println!(
        "cargo:rustc-env={}_SOURCE_OFFER={}",
        &pkg_name,
        crate_path
            .to_str()
            .expect("Path should be encodable as string")
    );

    let file = File::open(&crate_path).expect("Crate should be openable");
    let reader = GzDecoder::new(BufReader::new(file));
    let mut a = Archive::new(reader);
    for file in a.entries().expect("Crate should be readable as tar.gz") {
        let entry = file.expect("Crate file entries should be readable");
        let path = entry.path().expect("Crate file paths should be encodable");
        eprintln!("path: {:?}, prefix: {:?}", &path, &crate_name);
        let path = path
            .strip_prefix(&crate_name)
            .expect("Crate file paths should start with crate name");
        if path.exists() {
            println!("cargo:rerun-if-changed={}", path.to_str().unwrap());
        }
    }
}
