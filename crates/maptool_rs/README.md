# MapTool client library

This library provides interfaces to build MapTool clients.

[`crate::proto`] contains the generated protobuf interface.

[`crate::handshake::statemachine`] is the low-level interface to handle the handshake protocol.
It does not handle IO or cryptography and is intended as a building block for higher-level interfaces.

[`crate::types`] contains types used by the handshake protocol and may implement serde
to allow the values to be sent between processes.

[`crate::crypto`] contains helpers for creating ciphers and keys to use the appropriate algorithms
and extension traits for [`crate::types::challenge`] to conveniently turn them into responses.

The separation is to enable handling the handshake from another process
or private keys stored in TPMs or the kernel keyring.
