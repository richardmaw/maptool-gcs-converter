use std::borrow::{Borrow, Cow};
use std::marker::PhantomData;

use serde::{Deserialize, Serialize};
use thiserror::Error;

use super::{Error as ChallengeError, Salt, IV};

// TODO: the uses are irreconcilable:
// 1. The structure is generic and contains either references or the value
// 2. The structure supports serde
// 3. Deserializing borrows from the input where possible
// Choose two.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SymmetricChallenge<'name, 'salt, 'iv, 'cts, 'ct> {
    #[serde(borrow)]
    pub(crate) name: Cow<'name, str>,
    #[serde(borrow)]
    pub(crate) salt: Cow<'salt, Salt>,
    #[serde(borrow)]
    pub(crate) iv: Cow<'iv, IV>,
    #[serde(borrow)]
    pub(crate) ciphertexts: Cow<'cts, [Cow<'ct, [u8]>]>,
}

impl<'name, 'salt, 'iv, 'cts, 'ct>
    SymmetricChallenge<'name, 'salt, 'iv, 'cts, 'ct>
{
    pub(crate) fn new<Name, IntoSalt, IntoIV, Ciphertexts, Ciphertext>(
        name: Name,
        salt: IntoSalt,
        iv: IntoIV,
        ciphertexts: Ciphertexts,
    ) -> Self
    where
        Name: Into<Cow<'name, str>>,
        IntoSalt: Into<Cow<'salt, Salt>>,
        IntoIV: Into<Cow<'iv, IV>>,

        Ciphertext: Into<Cow<'ct, [u8]>>,
        Ciphertexts: IntoIterator<Item = Ciphertext>,
    {
        Self {
            name: name.into(),
            salt: salt.into(),
            iv: iv.into(),
            ciphertexts: ciphertexts.into_iter().map(Into::into).collect(),
        }
    }

    pub(crate) fn iter_ciphertexts(&self) -> impl Iterator<Item = &[u8]> {
        let ciphertexts: &[_] = self.ciphertexts.borrow();
        ciphertexts.into_iter().map(AsRef::as_ref)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn construct_owned() {
        let name = "test".to_string();
        let salt = Salt::from([1u8; 16]);
        let iv = IV::from([1u8; 16]);
        let ciphertexts: Vec<Vec<u8>> = vec![vec![0u8; 16]];
        let owned_symmetric_challenge =
            SymmetricChallenge::new(name, salt, iv, ciphertexts);
        for ciphertext in owned_symmetric_challenge.iter_ciphertexts() {
            assert_eq!(ciphertext, &[0u8; 16]);
        }
    }
    #[test]
    fn construct_borrowed() {
        let name = "test";
        let salt = [1u8; 16];
        let salt: &Salt = (&salt).into();
        let iv = [1u8; 16];
        let iv: &IV = (&iv).into();
        let ciphertexts_storage: [Vec<u8>; 1] = [vec![0u8; 16]];
        let ciphertexts = [&ciphertexts_storage[0][..]];
        let borrowed_symmetric_challenge =
            SymmetricChallenge::new(name, salt, iv, ciphertexts);
        for ciphertext in borrowed_symmetric_challenge.iter_ciphertexts() {
            let ciphertext: &[u8] = ciphertext.as_ref();
            assert_eq!(ciphertext, &[0u8; 16]);
        }
    }
    #[test]
    fn construct_slices() {
        let name = "test";
        let block = [1u8; 16];
        let salt: &Salt = block.as_slice().try_into().unwrap();
        let iv: &IV = block.as_slice().try_into().unwrap();
        let ciphertexts_storage: [Vec<u8>; 1] = [vec![0u8; 16]];
        let ciphertexts = [&ciphertexts_storage[0][..]];
        let borrowed_symmetric_challenge =
            SymmetricChallenge::new(name, salt, iv, ciphertexts);
        for ciphertext in borrowed_symmetric_challenge.iter_ciphertexts() {
            let ciphertext: &[u8] = ciphertext.as_ref();
            assert_eq!(ciphertext, &[0u8; 16]);
        }
    }
    #[test]
    fn serde() {
        let name = "test";
        let block = [1u8; 16];
        let salt: &Salt = block.as_slice().try_into().unwrap();
        let iv: &IV = block.as_slice().try_into().unwrap();
        let challenges = [[0u8; 16]];
        let challenge = SymmetricChallenge::new(
            name,
            salt,
            iv,
            challenges.iter().map(AsRef::as_ref),
        );
        let serialized = serde_json::to_string(&challenge).unwrap();
        assert_eq!(
            &serialized,
            "{\"name\":\"test\",\
            \"salt\":[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],\
            \"iv\":[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],\
            \"ciphertexts\":[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]}",
        );
        let deserialized: SymmetricChallenge =
            serde_json::from_str(&serialized).unwrap();
        assert!(matches!(deserialized.name, Cow::Borrowed("test")));
    }
}
