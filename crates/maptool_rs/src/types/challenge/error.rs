use thiserror::Error;

#[non_exhaustive]
#[derive(Debug, Error)]
pub enum Error {
    #[error("Invalid Salt length")]
    InvalidSaltLength,
    #[error("Invalid IV length")]
    InvalidIVLength,
}
