//! Convenience wrapper type for handshake challenge salts to ensure their length.
//!
//! This handles length checks as part of type conversion and uses a standard error type.

use core::mem;
use std::{
    borrow::Cow,
    ops::{Deref, DerefMut},
};

use serde::{Deserialize, Serialize};

use super::{Block, Error};

#[repr(transparent)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct Salt(Block);

impl Deref for Salt {
    type Target = Block;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Salt {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Block> for Salt {
    fn from(block: Block) -> Self {
        Self(block)
    }
}

impl Into<Block> for Salt {
    fn into(self) -> Block {
        self.0
    }
}

impl<'a> TryFrom<&'a mut [u8]> for &'a mut Salt {
    type Error = Error;
    fn try_from(slice: &'a mut [u8]) -> Result<Self, Self::Error> {
        let array_ref: &'a mut Block =
            slice.try_into().map_err(|_| Error::InvalidSaltLength)?;
        // Safety: Block is the sole member of Salt and uses repr(transparent)
        Ok(unsafe { mem::transmute::<&'a mut Block, &'a mut Salt>(array_ref) })
    }
}

impl<'a> Into<&'a mut [u8]> for &'a mut Salt {
    fn into(self) -> &'a mut [u8] {
        self.as_mut_slice()
    }
}

impl<'a> TryFrom<&'a [u8]> for &'a Salt {
    type Error = Error;
    fn try_from(slice: &'a [u8]) -> Result<Self, Self::Error> {
        let array_ref: &'a Block =
            slice.try_into().map_err(|_| Error::InvalidSaltLength)?;
        // Safety: Block is the sole member of Salt and uses repr(transparent)
        Ok(unsafe { mem::transmute::<&'a Block, &'a Salt>(array_ref) })
    }
}

impl<'a> Into<&'a [u8]> for &'a Salt {
    fn into(self) -> &'a [u8] {
        self.as_slice()
    }
}

impl<'a> From<&'a mut Block> for &'a mut Salt {
    fn from(array_ref: &'a mut Block) -> Self {
        // Safety: Block is the sole member of Salt and uses repr(transparent)
        unsafe { mem::transmute::<&'a mut Block, &'a mut Salt>(array_ref) }
    }
}

impl<'a> Into<&'a mut Block> for &'a mut Salt {
    fn into(self) -> &'a mut Block {
        &mut *self
    }
}

impl<'a> From<&'a Block> for &'a Salt {
    fn from(array_ref: &'a Block) -> Self {
        // Safety: Block is the sole member of Salt and uses repr(transparent)
        unsafe { mem::transmute::<&'a Block, &'a Salt>(array_ref) }
    }
}

impl<'a> Into<&'a Block> for &'a Salt {
    fn into(self) -> &'a Block {
        &*self
    }
}

impl<'a> From<Cow<'a, Salt>> for Salt {
    fn from(value: Cow<'a, Salt>) -> Self {
        value.into_owned()
    }
}

impl Into<Cow<'static, Salt>> for Salt {
    fn into(self) -> Cow<'static, Salt> {
        Cow::Owned(self)
    }
}

impl<'a> Into<Cow<'a, Salt>> for &'a Salt {
    fn into(self) -> Cow<'a, Salt> {
        Cow::Borrowed(self)
    }
}
