#[derive(Clone, Debug)]
pub struct AsymmetricChallenge<Name, Bytes>
where
    Name: AsRef<str>,
    Bytes: AsRef<[u8]>,
{
    pub(crate) name: Name,
    pub(crate) ciphertext: Bytes,
}
