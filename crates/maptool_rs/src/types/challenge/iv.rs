//! Convenience wrapper type for handshake challenge IVs to ensure their length.
//!
//! This handles length checks as part of type conversion and uses a standard error type.

use core::mem;
use std::{
    borrow::Cow,
    ops::{Deref, DerefMut},
};

use serde::{Deserialize, Serialize};

use super::{Block, Error};

#[repr(transparent)]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct IV(Block);

impl Deref for IV {
    type Target = Block;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for IV {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Block> for IV {
    fn from(block: Block) -> Self {
        Self(block)
    }
}

impl Into<Block> for IV {
    fn into(self) -> Block {
        self.0
    }
}

impl<'a> TryFrom<&'a mut [u8]> for &'a mut IV {
    type Error = Error;
    fn try_from(slice: &'a mut [u8]) -> Result<Self, Self::Error> {
        let array_ref: &'a mut Block =
            slice.try_into().map_err(|_| Error::InvalidIVLength)?;
        // Safety: Block is the sole member of IV and uses repr(transparent)
        Ok(unsafe { mem::transmute::<&'a mut Block, &'a mut IV>(array_ref) })
    }
}

impl<'a> Into<&'a mut [u8]> for &'a mut IV {
    fn into(self) -> &'a mut [u8] {
        self.as_mut_slice()
    }
}

impl<'a> TryFrom<&'a [u8]> for &'a IV {
    type Error = Error;
    fn try_from(slice: &'a [u8]) -> Result<Self, Self::Error> {
        let array_ref: &'a Block =
            slice.try_into().map_err(|_| Error::InvalidIVLength)?;
        // Safety: Block is the sole member of IV and uses repr(transparent)
        Ok(unsafe { mem::transmute::<&'a Block, &'a IV>(array_ref) })
    }
}

impl<'a> Into<&'a [u8]> for &'a IV {
    fn into(self) -> &'a [u8] {
        self.as_slice()
    }
}

impl<'a> From<&'a mut Block> for &'a mut IV {
    fn from(array_ref: &'a mut Block) -> Self {
        // Safety: Block is the sole member of IV and uses repr(transparent)
        unsafe { mem::transmute::<&'a mut Block, &'a mut IV>(array_ref) }
    }
}

impl<'a> Into<&'a mut Block> for &'a mut IV {
    fn into(self) -> &'a mut Block {
        &mut *self
    }
}

impl<'a> From<&'a Block> for &'a IV {
    fn from(array_ref: &'a Block) -> Self {
        // Safety: Block is the sole member of IV and uses repr(transparent)
        unsafe { mem::transmute::<&'a Block, &'a IV>(array_ref) }
    }
}

impl<'a> Into<&'a Block> for &'a IV {
    fn into(self) -> &'a Block {
        &*self
    }
}

impl<'a> From<Cow<'a, IV>> for IV {
    fn from(value: Cow<'a, IV>) -> Self {
        value.into_owned()
    }
}

impl Into<Cow<'static, IV>> for IV {
    fn into(self) -> Cow<'static, IV> {
        Cow::Owned(self)
    }
}

impl<'a> Into<Cow<'a, IV>> for &'a IV {
    fn into(self) -> Cow<'a, IV> {
        Cow::Borrowed(self)
    }
}
