use std::borrow::Cow;

use pkcs5::pbes2::{
    EncryptionScheme as Pbes2EncryptionScheme, Parameters as Pbes2Parameters,
    Pbkdf2Params, Pbkdf2Prf,
};
use rsa::RsaPublicKey;

const KEY_ITERATION_KEY_COUNT: u32 = 2000;
pub(crate) const BLOCK_SIZE: usize = 16;

pub(crate) fn make_symmetric_cipher<'a>(
    salt: &'a [u8],
    iv: &'a [u8; 16],
) -> Pbes2Parameters<'a> {
    Pbes2Parameters {
        kdf: Pbkdf2Params {
            salt,
            iteration_count: KEY_ITERATION_KEY_COUNT,
            key_length: None,
            prf: Pbkdf2Prf::HmacWithSha1,
        }
        .into(),
        encryption: Pbes2EncryptionScheme::Aes128Cbc { iv },
    }
}

pub(crate) struct AsymmetricEncryptor<'a> {
    public_key: Cow<'a, RsaPublicKey>,
}

impl<'a> AsymmetricEncryptor<'a> {
    pub(crate) fn encrypt<M>(&self, message: M) -> Vec<u8>
    where
        M: AsRef<[u8]>,
    {
        self.public_key.encrypt(
            &mut crate::rand::rngs::OsRng,
            rsa::Pkcs1v15Encrypt,
            message.as_ref(),
        ).expect("Encryption with fixed parameters and a valid key should be infallible")
    }
}

pub(crate) fn make_asymmetric_encryptor<'a>(
    public_key: impl Into<Cow<'a, RsaPublicKey>>,
) -> AsymmetricEncryptor<'a> {
    AsymmetricEncryptor {
        public_key: public_key.into(),
    }
}
