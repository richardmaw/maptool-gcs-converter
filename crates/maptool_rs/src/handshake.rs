use base64ct::{Base64, Encoding};
use futures::{AsyncReadExt, AsyncWriteExt};
use thiserror::Error;

pub mod statemachine;

pub(crate) fn md5sum_pubkey<K>(key: &K) -> String
where
    K: AsRef<[u8]>,
{
    let mut hasher = md5::Context::new();
    hasher.consume("======BeginPublicKey======");
    for chunk in key.as_ref().chunks(60) {
        let mut enc_buf = [0u8; 80];
        let encoded = Base64::encode(chunk, &mut enc_buf)
            .expect("60 byte chunks encode to 80 bytes");
        hasher.consume(encoded);
    }
    hasher.consume("======EndPublicKey======");
    let digest = hasher.compute();
    hex::encode(*digest)
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum ReadMsgError {
    #[error("Failed to read message length")]
    ReadLengthFailed(#[source] futures_io::Error),
    #[error("Failed to read message payload")]
    ReadPayloadFailed(#[source] futures_io::Error),
}

async fn read_msg<C>(
    mut conn: C,
    msgbuf: &mut Vec<u8>,
) -> Result<(), ReadMsgError>
where
    C: futures_io::AsyncRead + Unpin,
{
    let mut lenbuf = [0u8; 4];
    conn.read_exact(&mut lenbuf)
        .await
        .map_err(ReadMsgError::ReadLengthFailed)?;
    let len = u32::from_be_bytes(lenbuf);
    msgbuf.resize(len as usize, 0);
    conn.read_exact(msgbuf.as_mut_slice())
        .await
        .map_err(ReadMsgError::ReadPayloadFailed)?;
    Ok(())
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum WriteMsgError {
    #[error("Failed to write message length")]
    WriteLengthFailed(#[source] futures_io::Error),
    #[error("Failed to write message payload")]
    WritePayloadFailed(#[source] futures_io::Error),
}

async fn write_msg<C>(mut conn: C, msgbuf: &[u8]) -> Result<(), WriteMsgError>
where
    C: futures_io::AsyncWrite + Unpin,
{
    let msg_len = msgbuf.len() as u32;
    conn.write(&msg_len.to_be_bytes())
        .await
        .map_err(WriteMsgError::WriteLengthFailed)?;
    conn.write(msgbuf)
        .await
        .map_err(WriteMsgError::WritePayloadFailed)?;
    Ok(())
}

pub struct Session<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    conn: C,
}

pub struct ReadSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    rx: futures::io::ReadHalf<C>,
}

pub struct WriteSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite,
{
    tx: futures::io::WriteHalf<C>,
}

// TODO: Should instead implement Sink<Bytes> and Stream<Item=BytesMut>
// but also when the protocol's types are available should rewrite to implement
// Sink<Message> and Stream<Result<Message, DeserializeError>> type
impl<C> Session<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn read_msg(
        &mut self,
        mut msgbuf: Vec<u8>,
    ) -> Result<Vec<u8>, ReadMsgError> {
        msgbuf.clear();
        read_msg(&mut self.conn, &mut msgbuf).await?;
        Ok(msgbuf)
    }

    pub async fn write_msg(
        &mut self,
        mut msgbuf: Vec<u8>,
    ) -> Result<Vec<u8>, WriteMsgError> {
        write_msg(&mut self.conn, &mut msgbuf).await?;
        Ok(msgbuf)
    }

    pub fn split(self) -> (ReadSession<C>, WriteSession<C>) {
        let (rx, tx) = self.conn.split();
        (ReadSession { rx }, WriteSession { tx })
    }
}

impl<C> ReadSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn read_msg(
        &mut self,
        mut msgbuf: Vec<u8>,
    ) -> Result<Vec<u8>, ReadMsgError> {
        msgbuf.clear();
        read_msg(&mut self.rx, &mut msgbuf).await?;
        Ok(msgbuf)
    }
}

impl<C> WriteSession<C>
where
    C: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn write_msg(
        &mut self,
        mut msgbuf: Vec<u8>,
    ) -> Result<Vec<u8>, WriteMsgError> {
        write_msg(&mut self.tx, &mut msgbuf).await?;
        Ok(msgbuf)
    }
}
