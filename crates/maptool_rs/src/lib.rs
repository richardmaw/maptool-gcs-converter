#![doc = include_str!("../README.md")]

use core::{
    fmt,
    pin::Pin,
    task::{Context, Poll},
};
use std::{
    io::{Error as IoError, ErrorKind as IoErrorKind},
    net::ToSocketAddrs,
};

pub use rand;
pub use rsa;

use async_trait::async_trait;
use base64ct::{Base64, Encoding};
use rsa::{pkcs1::DecodeRsaPublicKey, RsaPublicKey};
use thiserror::Error;
use tokio::net::TcpStream;

pub(crate) mod crypto;

pub mod types;
pub use types::{DirectServerAddress, ServerAddress, WebRtcServerAddress};

pub const SOURCE_OFFER: &'static [u8] =
    include_bytes!(env!(concat!(env!("CARGO_PKG_NAME"), "_SOURCE_OFFER")));

pub mod handshake;
mod proto {
    include!(concat!(env!("OUT_DIR"), "/proto/mod.rs"));
}
pub mod webrtc;

pub(crate) const VERSION: &'static str = "1.12.0";

pub struct MapToolFormattedPublicKey<'a> {
    key: &'a [u8],
}

const KEY_PREFIX: &'static str = "====== Begin Public Key ======";
const KEY_SUFFIX: &'static str = "====== End Public Key ======";
const LINE_WRAP: usize = 80;

impl<'a> fmt::Display for MapToolFormattedPublicKey<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}\n", KEY_PREFIX)?;
        for chunk in self.key.chunks(60) {
            let mut enc_buf = [0u8; LINE_WRAP];
            let encoded = Base64::encode(chunk, &mut enc_buf)
                .expect("60 byte chunks encode to 80 bytes");
            write!(f, "{}\n", encoded)?;
        }
        write!(f, "{}\n", KEY_SUFFIX)
    }
}

pub fn format_pubkey<'a, K>(key: &'a K) -> MapToolFormattedPublicKey<'a>
where
    K: AsRef<[u8]>,
{
    MapToolFormattedPublicKey { key: key.as_ref() }
}

#[derive(Debug, Error)]
pub enum ParsePubKeyError {
    #[error("The key did not start with the expected header")]
    InvalidPrefix,
    #[error("The key did not end with the expected footer")]
    InvalidSuffix,
    #[error("The contents between headers was not valid standard base64")]
    InvalidBase64(#[from] base64ct::Error),
    #[error("The decoded contents was not a valid pkcs1 public key DER")]
    InvalidPubKey(#[from] rsa::pkcs1::Error),
}

// TODO: Move to crypto module?
pub fn parse_pubkey<I>(input: I) -> Result<RsaPublicKey, ParsePubKeyError>
where
    I: AsRef<str>,
{
    use ParsePubKeyError::*;
    let s = input
        .as_ref()
        .strip_prefix(KEY_PREFIX)
        .ok_or(InvalidPrefix)?
        .trim_start();
    let s = s.trim_end().strip_suffix(KEY_SUFFIX).ok_or(InvalidSuffix)?;
    // / and + in key, standard base64ct
    let mut bytes = Vec::new();
    let mut decoder = base64ct::Decoder::<base64ct::Base64>::new_wrapped(
        s.as_bytes(),
        LINE_WRAP,
    )?;
    decoder.decode_to_end(&mut bytes)?;
    Ok(RsaPublicKey::from_pkcs1_der(&bytes)?)
}

#[pin_project::pin_project(project = ServerConnectionProj)]
pub enum ServerConnection {
    Tcp(#[pin] TcpStream),
    DataChannel(#[pin] webrtc::SingletonWebRtcDataChannelMessageStream),
}

#[async_trait]
pub trait Connect {
    type Error;
    async fn connect(&self) -> Result<ServerConnection, Self::Error>;
}

#[async_trait]
impl<DirectAddress> Connect for DirectServerAddress<DirectAddress>
where
    DirectAddress: ToSocketAddrs + Send + Sync,
{
    type Error = IoError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        // TODO: This blocks to resolve names,
        // but tokio::net::ToSocketAddrs is executor specific
        // NOTE: ToSocketAddrs::Iter isn't send,
        // so it's collected into a Vec whose iter is.
        let addrs: Vec<_> = self.address.to_socket_addrs()?.collect();

        let mut r = Err(IoError::new(
            IoErrorKind::InvalidInput,
            "could not resolve to any addresses",
        ));
        for addr in addrs {
            r = TcpStream::connect(addr).await;
            if r.is_ok() {
                break;
            }
        }

        Ok(ServerConnection::Tcp(r?))
    }
}

#[async_trait]
impl<Name, ServerName> Connect for WebRtcServerAddress<Name, ServerName>
where
    Name: AsRef<str> + Send + Sync,
    ServerName: AsRef<str> + Send + Sync,
{
    type Error = webrtc::ConnectError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        let conn = webrtc::Signaller::builder()
            .id(self.name.as_ref())
            .server_name(self.server_name.as_ref())
            .build()
            .connect()
            .await?;
        Ok(ServerConnection::DataChannel(conn))
    }
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum ServerConnectError {
    #[error("Failed to connect to TCP socket")]
    TcpConnectFailed(#[source] IoError),
    #[error("Failed to connect to WebRTC DataChannel")]
    WebRtcConnectFailed(#[source] webrtc::ConnectError),
}

#[async_trait]
impl<DirectAddress, Name, ServerName> Connect
    for ServerAddress<DirectAddress, Name, ServerName>
where
    DirectAddress: ToSocketAddrs + Send + Sync,
    Name: AsRef<str> + Send + Sync,
    ServerName: AsRef<str> + Send + Sync,
{
    type Error = ServerConnectError;
    async fn connect(&self) -> Result<ServerConnection, Self::Error> {
        use ServerConnectError::*;
        match self {
            ServerAddress::Direct(address) => {
                Ok(address.connect().await.map_err(TcpConnectFailed)?)
            }
            ServerAddress::WebRtc(address) => {
                Ok(address.connect().await.map_err(WebRtcConnectFailed)?)
            }
        }
    }
}

impl ServerConnection {
    pub async fn connect<C>(address: C) -> Result<Self, C::Error>
    where
        C: Connect,
    {
        address.connect().await
    }
}

impl tokio::io::AsyncRead for ServerConnection {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_read(cx, buf),
            ServerConnectionProj::DataChannel(d) => d.poll_read(cx, buf),
        }
    }
}

impl tokio::io::AsyncWrite for ServerConnection {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_write(cx, buf),
            ServerConnectionProj::DataChannel(d) => d.poll_write(cx, buf),
        }
    }
    fn poll_flush(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_flush(cx),
            ServerConnectionProj::DataChannel(d) => d.poll_flush(cx),
        }
    }
    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<std::io::Result<()>> {
        match self.project() {
            ServerConnectionProj::Tcp(s) => s.poll_shutdown(cx),
            ServerConnectionProj::DataChannel(d) => d.poll_shutdown(cx),
        }
    }
}
