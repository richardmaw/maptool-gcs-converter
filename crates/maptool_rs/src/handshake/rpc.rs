use core::{convert::TryInto, fmt::Write};

use bytes::{Bytes, BytesMut};
use protobuf::{error::ProtobufError, Chars, Message};
use thiserror::Error;

use crate::types::{
    Challenge, ChallengeData, ChallengeUnmarshallError, HandshakeKind,
};

use crate::handshake::{
    md5sum_pubkey, read_msg, write_msg, ReadMsgError, Session, WriteMsgError,
};
use crate::proto;

pub struct PendingResponse<Connection>
where
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    connection: Connection,
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum RespondError {
    #[error("Failed to serialize ClientAuthMsg")]
    SerializeClientAuthMsgFailed(#[source] ProtobufError),
    #[error("Failed to write ClientAuthMsg")]
    WriteClientAuthMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read ClientAuthMsg response")]
    ReadClientAuthMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize ClientAuthMsg response")]
    DeserializeClientAuthMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected ClientAuthMsg response")]
    ClientAuthMsgResponseUnexpected(proto::handshake::HandshakeMsg),
}

impl<Connection> PendingResponse<Connection>
where
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    pub async fn respond<Iv, Response>(
        mut self,
        iv: Option<Iv>,
        response: Response,
    ) -> Result<
        (
            proto::handshake::ConnectionSuccessfulMsg,
            Session<Connection>,
        ),
        RespondError,
    >
    where
        Iv: AsRef<[u8]>,
        Response: AsRef<[u8]>,
    {
        use RespondError::*;
        let mut req_handshake_msg = proto::handshake::HandshakeMsg::default();
        let client_auth = req_handshake_msg.mut_client_auth_message();
        client_auth
            .set_challenge_response(Bytes::copy_from_slice(response.as_ref()));
        if let Some(iv) = iv {
            client_auth.set_iv(Bytes::copy_from_slice(iv.as_ref()));
        }

        log::debug!("Sending {:?}", &req_handshake_msg);
        let mut msgbuf = Vec::new();
        req_handshake_msg
            .write_to_vec(&mut msgbuf)
            .map_err(SerializeClientAuthMsgFailed)?;
        write_msg(&mut self.connection, &msgbuf)
            .await
            .map_err(WriteClientAuthMsgFailed)?;

        log::debug!("Awaiting response");
        read_msg(&mut self.connection, &mut msgbuf)
            .await
            .map_err(ReadClientAuthMsgResponseFailed)?;
        let mut rsp_handshake_msg =
            proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
                .map_err(DeserializeClientAuthMsgResponseFailed)?;
        log::debug!("Response {:?}", &rsp_handshake_msg);

        if !rsp_handshake_msg.has_connection_successful_msg() {
            return Err(ClientAuthMsgResponseUnexpected(rsp_handshake_msg));
        }

        Ok((
            rsp_handshake_msg.take_connection_successful_msg(),
            Session {
                conn: self.connection,
            },
        ))
    }
}

pub struct PendingApproval<Connection, PublicKey>
where
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
    PublicKey: AsRef<[u8]> + Sized,
{
    connection: Connection,
    handshake_msg: proto::handshake::HandshakeMsg,
    public_key: PublicKey,
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum WaitForApprovalError {
    #[error("Failed to serialize PublicKeyUploadMsg")]
    SerializePublicKeyUploadMsgFailed(#[source] ProtobufError),
    #[error("Failed to write PublicKeyUploadMsg")]
    WritePublicKeyUploadMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read PublicKeyUploadMsg response")]
    ReadPublicKeyUploadMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize PublicKeyUploadMsg response")]
    DeserializePublicKeyUploadMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected PublicKeyAdded response")]
    PublicKeyUploadMsgResponseUnexpected(proto::handshake::HandshakeMsg),
    #[error("PublicKeyAdded response did not match sent key")]
    PublicKeyUploadMsgResponseKeyMismatch { sent: Chars, received: Chars },
    #[error("Failed to serialize ClientInitMsg")]
    SerializeClientInitMsgFailed(#[source] ProtobufError),
    #[error("Failed to write ClientInitMsg")]
    WriteClientInitMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read ClientInitMsg response")]
    ReadClientInitMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize ClientInitMsg response")]
    DeserializeClientInitMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected ClientInitMsg response")]
    ClientInitMsgResponseUnexpected(proto::handshake::HandshakeMsg),
    #[error("Provided challenge is invalid")]
    ChallengeInvalid(#[source] ChallengeUnmarshallError),
}

impl<Connection, PublicKey> PendingApproval<Connection, PublicKey>
where
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
    PublicKey: AsRef<[u8]> + Sized,
{
    pub async fn wait_for_approval(
        mut self,
    ) -> Result<(PendingResponse<Connection>, Challenge), WaitForApprovalError>
    {
        use WaitForApprovalError::*;

        let mut maptool_pubkey_bytes = BytesMut::new();
        write!(
            &mut maptool_pubkey_bytes,
            "{}",
            crate::format_pubkey(&self.public_key)
        )
        .expect("Serialized pubkey should be shorter than maximum buffer size");
        let maptool_pubkey_text =
            Chars::from_bytes(maptool_pubkey_bytes.freeze())
                .expect("format_pubkey should serialize utf8");
        let mut req_handshake_msg = proto::handshake::HandshakeMsg::default();
        let public_key_upload_msg =
            req_handshake_msg.mut_public_key_upload_msg();
        public_key_upload_msg.set_public_key(maptool_pubkey_text);

        log::debug!("Sending {:?}", &req_handshake_msg);
        let mut msgbuf = Vec::new();
        req_handshake_msg
            .write_to_vec(&mut msgbuf)
            .map_err(SerializePublicKeyUploadMsgFailed)?;
        write_msg(&mut self.connection, &msgbuf)
            .await
            .map_err(WritePublicKeyUploadMsgFailed)?;

        let pubkey_text = req_handshake_msg
            .mut_public_key_upload_msg()
            .take_public_key();
        log::debug!("Awaiting response");
        read_msg(&mut self.connection, &mut msgbuf)
            .await
            .map_err(ReadPublicKeyUploadMsgResponseFailed)?;
        let mut rsp_handshake_msg =
            proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
                .map_err(DeserializePublicKeyUploadMsgResponseFailed)?;
        log::debug!("Response: {:?}", &rsp_handshake_msg);

        if !rsp_handshake_msg.has_public_key_added_msg() {
            return Err(PublicKeyUploadMsgResponseUnexpected(
                rsp_handshake_msg,
            ));
        }

        let response_pubkey_text = rsp_handshake_msg
            .mut_public_key_added_msg()
            .take_public_key();
        if response_pubkey_text != pubkey_text {
            return Err(PublicKeyUploadMsgResponseKeyMismatch {
                sent: pubkey_text,
                received: response_pubkey_text,
            });
        }

        log::debug!("Sending {:?}", &req_handshake_msg);
        msgbuf.clear();
        self.handshake_msg
            .write_to_vec(&mut msgbuf)
            .map_err(SerializeClientInitMsgFailed)?;
        write_msg(&mut self.connection, &msgbuf)
            .await
            .map_err(WriteClientInitMsgFailed)?;

        log::debug!("Awaiting response");
        read_msg(&mut self.connection, &mut msgbuf)
            .await
            .map_err(ReadClientInitMsgResponseFailed)?;
        rsp_handshake_msg =
            proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
                .map_err(DeserializeClientInitMsgResponseFailed)?;
        log::debug!("Response: {:?}", &rsp_handshake_msg);

        if !rsp_handshake_msg.has_use_auth_type_msg() {
            return Err(ClientInitMsgResponseUnexpected(rsp_handshake_msg));
        }
        let mut use_auth_type = rsp_handshake_msg.take_use_auth_type_msg();
        let kind = match use_auth_type.get_auth_type() {
            proto::handshake::AuthTypeEnum::ASYMMETRIC_KEY => {
                HandshakeKind::Asymmetric
            }
            proto::handshake::AuthTypeEnum::SHARED_PASSWORD => {
                HandshakeKind::Symmetric
            }
        };
        let challenge_data = ChallengeData {
            kind: kind,
            salt: use_auth_type.take_salt(),
            iv: use_auth_type.take_iv(),
            ciphertexts: use_auth_type.take_challenge(),
        };
        Ok((
            PendingResponse {
                connection: self.connection,
            },
            challenge_data.try_into().map_err(ChallengeInvalid)?,
        ))
    }
}

pub enum InitState<Connection, PublicKey>
where
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
    PublicKey: AsRef<[u8]> + Sized,
{
    PendingApproval {
        connection: PendingApproval<Connection, PublicKey>,
        pin: Chars,
    },
    PendingResponse {
        challenge: Challenge,
        connection: PendingResponse<Connection>,
    },
}

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum InitError {
    #[error("Failed to serialize ClientInitMsg")]
    SerializeClientInitMsgFailed(#[source] ProtobufError),
    #[error("Failed to write ClientInitMsg")]
    WriteClientInitMsgFailed(#[source] WriteMsgError),
    #[error("Failed to read ClientInitMsg response")]
    ReadClientInitMsgResponseFailed(#[source] ReadMsgError),
    #[error("Failed to deserialize ClientInitMsg response")]
    DeserializeClientInitMsgResponseFailed(#[source] ProtobufError),
    #[error("Did not receive expected ClientInitMsg response")]
    ClientInitMsgResponseUnexpected(proto::handshake::HandshakeMsg),
    #[error("Public Key was requested and none was provided")]
    PublicKeyRequested,
    #[error("Provided challenge is invalid")]
    ChallengeInvalid(#[source] ChallengeUnmarshallError),
}

pub async fn init<Connection, PublicKey>(
    mut connection: Connection,
    name: &str,
    public_key: Option<PublicKey>,
) -> Result<InitState<Connection, PublicKey>, InitError>
where
    PublicKey: AsRef<[u8]> + Sized,
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
{
    use InitError::*;
    let mut msgbuf = Vec::new();

    let public_key_md5 = public_key
        .as_ref()
        .map_or_else(Chars::new, |key| md5sum_pubkey(key).into());

    let mut handshake_msg = proto::handshake::HandshakeMsg::default();
    let client_init_msg = handshake_msg.mut_client_init_msg();
    client_init_msg.set_player_name(Chars::from(name.as_ref()));
    client_init_msg.set_version(
        Chars::from_bytes(Bytes::from_static(crate::VERSION.as_bytes()))
            .expect("Chars initialized from str should be valid UTF8"),
    );
    client_init_msg.set_public_key_md5(public_key_md5);

    log::debug!("Sending {:?}", &client_init_msg);
    msgbuf.clear();
    handshake_msg
        .write_to_vec(&mut msgbuf)
        .map_err(SerializeClientInitMsgFailed)?;
    write_msg(&mut connection, &msgbuf)
        .await
        .map_err(WriteClientInitMsgFailed)?;

    log::debug!("Awaiting response");
    read_msg(&mut connection, &mut msgbuf)
        .await
        .map_err(ReadClientInitMsgResponseFailed)?;
    let mut rsp_handshake_msg =
        proto::handshake::HandshakeMsg::parse_from_bytes(&msgbuf)
            .map_err(DeserializeClientInitMsgResponseFailed)?;
    log::debug!("Response: {:?}", &rsp_handshake_msg);

    if rsp_handshake_msg.has_use_auth_type_msg() {
        let mut use_auth_type = rsp_handshake_msg.take_use_auth_type_msg();
        let kind = match use_auth_type.get_auth_type() {
            proto::handshake::AuthTypeEnum::ASYMMETRIC_KEY => {
                HandshakeKind::Asymmetric
            }
            proto::handshake::AuthTypeEnum::SHARED_PASSWORD => {
                HandshakeKind::Symmetric
            }
        };
        let challenge_data = ChallengeData {
            kind: kind,
            salt: use_auth_type.take_salt(),
            iv: use_auth_type.take_iv(),
            ciphertexts: use_auth_type.take_challenge(),
        };
        return Ok(InitState::PendingResponse {
            connection: PendingResponse {
                connection: connection,
            },
            challenge: challenge_data.try_into().map_err(ChallengeInvalid)?,
        });
    }
    if !rsp_handshake_msg.has_request_public_key_msg() {
        return Err(ClientInitMsgResponseUnexpected(rsp_handshake_msg));
    }
    let public_key = public_key.ok_or(PublicKeyRequested)?;

    let pin = rsp_handshake_msg.take_request_public_key_msg().take_pin();
    Ok(InitState::PendingApproval {
        connection: PendingApproval {
            connection,
            handshake_msg,
            public_key,
        },
        pin: pin,
    })
}
