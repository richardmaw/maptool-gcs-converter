use super::handshake_error::HandshakeError;
use crate::proto::handshake::{
    ConnectionSuccessfulMsg, HandshakeMsg, HandshakeResponseCodeMsg,
};

pub struct AwaitingConnectionSuccessful;
impl AwaitingConnectionSuccessful {
    pub fn handle_connection_successful_msg(
        self,
        connection_successful_msg: ConnectionSuccessfulMsg,
    ) -> Session {
        Session {
            connection_successful_msg,
        }
    }

    pub fn handle_handshake_response_code_msg(
        self,
        handshake_response_code_msg: HandshakeResponseCodeMsg,
    ) -> Result<(), HandshakeError> {
        Err(match handshake_response_code_msg {
            HandshakeResponseCodeMsg::INVALID_HANDSHAKE => {
                HandshakeError::InvalidHandshake
            }
            HandshakeResponseCodeMsg::INVALID_PASSWORD => {
                HandshakeError::InvalidPassword
            }
            HandshakeResponseCodeMsg::INVALID_PUBLIC_KEY => {
                HandshakeError::InvalidPublicKey
            }
            _ => {
                let mut handshake_msg = HandshakeMsg::new();
                handshake_msg.set_handshake_response_code_msg(
                    handshake_response_code_msg,
                );
                HandshakeError::UnexpectedMessage(handshake_msg)
            }
        })
    }
    pub fn handle_server_disconnect(self) -> Result<(), HandshakeError> {
        Err(HandshakeError::ServerDisconnected)
    }
}
// TODO: Move this out of handshake
pub struct Session {
    connection_successful_msg: ConnectionSuccessfulMsg,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn awaiting_connection_successful_server_disconnected() {
        let state = AwaitingConnectionSuccessful;
        assert!(matches!(
            state.handle_server_disconnect(),
            Err(HandshakeError::ServerDisconnected)
        ))
    }

    #[test]
    fn awaiting_connection_successful_invalid_handshake() {
        let state = AwaitingConnectionSuccessful;
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_HANDSHAKE,
            ),
            Err(HandshakeError::InvalidHandshake)
        ))
    }

    #[test]
    fn awaiting_connection_successful_invalid_password() {
        let state = AwaitingConnectionSuccessful;
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_PASSWORD,
            ),
            Err(HandshakeError::InvalidPassword)
        ))
    }

    #[test]
    fn awaiting_connection_successful_invalid_public_key() {
        let state = AwaitingConnectionSuccessful;
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_PUBLIC_KEY,
            ),
            Err(HandshakeError::InvalidPublicKey)
        ))
    }

    #[test]
    fn awaiting_connection_successful_connection_successful() {
        let state = AwaitingConnectionSuccessful;
        let connection_successful_msg = ConnectionSuccessfulMsg::default();
        assert!(matches!(
            state.handle_connection_successful_msg(connection_successful_msg),
            Session { .. }
        ));
    }
}
