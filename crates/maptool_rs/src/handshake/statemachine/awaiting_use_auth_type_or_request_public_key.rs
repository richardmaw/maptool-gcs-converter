use super::{
    awaiting_approval::AwaitingApproval,
    expecting_credential::ExpectingCredential,
    expecting_password::ExpectingPassword, handshake_error::HandshakeError,
};
use crate::proto::handshake::{
    AuthTypeEnum, HandshakeMsg, HandshakeResponseCodeMsg, PlayerBlockedMsg,
    PublicKeyUploadMsg, RequestPublicKeyMsg, UseAuthTypeMsg,
};

#[derive(Debug)]
pub struct AwaitingUseAuthTypeOrRequestPublicKey<Name, PublicKey = [u8; 0]>
where
    Name: AsRef<str>,
    PublicKey: AsRef<[u8]>,
{
    pub(crate) name: Name,
    pub(crate) public_key: Option<PublicKey>,
}

pub type PinCode = u16;
impl<Name, PublicKey> AwaitingUseAuthTypeOrRequestPublicKey<Name, PublicKey>
where
    Name: AsRef<str>,
    PublicKey: AsRef<[u8]>,
{
    pub fn handle_request_public_key_msg(
        self,
        request_public_key_msg: RequestPublicKeyMsg,
    ) -> Result<(AwaitingApproval, PinCode, PublicKeyUploadMsg), HandshakeError>
    {
        todo!("Use {request_public_key_msg:?}")
    }

    pub fn handle_use_auth_type_msg(
        self,
        mut use_auth_type_msg: UseAuthTypeMsg,
    ) -> Result<ExpectingCredential<Name>, HandshakeError> {
        match use_auth_type_msg.get_auth_type() {
            AuthTypeEnum::ASYMMETRIC_KEY => {
                todo!("Define helper function for handling this")
            }
            AuthTypeEnum::SHARED_PASSWORD => {
                let salt = use_auth_type_msg
                    .get_salt()
                    .try_into()
                    .map_err(|_| HandshakeError::InvalidChallengeSalt)?;
                let iv = use_auth_type_msg
                    .get_iv()
                    .try_into()
                    .map_err(|_| HandshakeError::InvalidChallengeIv)?;
                // The challenges are a vector of encrypted passwords with name prepended
                // in order of most privileged to least.
                // We _should_ only get two, GM then Player, or possibly a single per-player password
                // but we can be flexible and accept decrypting any number of them
                let challenges = use_auth_type_msg.take_challenge();
                // If a server provides no challenges there's no way to successfully authenticate
                // so this is an unrecoverable error requiring a disconnect.
                if challenges.len() == 0 {
                    return Err(HandshakeError::ServerOfferedNoChallenges);
                }
                Ok(ExpectingCredential::ExpectingPassword(ExpectingPassword {
                    name: self.name,
                    salt,
                    iv,
                    challenges,
                }))
            }
        }
    }

    pub fn handle_handshake_response_code_msg(
        self,
        handshake_response_code_msg: HandshakeResponseCodeMsg,
    ) -> Result<(), HandshakeError> {
        Err(match handshake_response_code_msg {
            HandshakeResponseCodeMsg::INVALID_HANDSHAKE => {
                HandshakeError::InvalidHandshake
            }
            HandshakeResponseCodeMsg::PLAYER_ALREADY_CONNECTED => {
                HandshakeError::PlayerAlreadyConnected
            }
            HandshakeResponseCodeMsg::WRONG_VERSION => {
                HandshakeError::WrongVersion
            }
            HandshakeResponseCodeMsg::INVALID_PASSWORD => {
                HandshakeError::UserDbUnloadableOrUserUnknown
            }
            HandshakeResponseCodeMsg::INVALID_PUBLIC_KEY => {
                HandshakeError::PublicKeyNotFound
            }
            _ => {
                let mut handshake_msg = HandshakeMsg::new();
                handshake_msg.set_handshake_response_code_msg(
                    handshake_response_code_msg,
                );
                HandshakeError::UnexpectedMessage(handshake_msg)
            }
        })
    }

    pub fn handle_player_blocked_msg(
        self,
        mut player_blocked_msg: PlayerBlockedMsg,
    ) -> Result<(), HandshakeError> {
        Err(HandshakeError::PlayerBlocked(
            player_blocked_msg.take_reason(),
        ))
    }

    pub fn handle_unexpected_message(
        self,
        handshake_msg: HandshakeMsg,
    ) -> Result<(), HandshakeError> {
        Err(HandshakeError::UnexpectedMessage(handshake_msg))
    }

    pub fn handle_server_disconnect(self) -> Result<(), HandshakeError> {
        Err(HandshakeError::ServerDisconnected)
    }
}

#[cfg(test)]
mod test {
    use bytes::Bytes;
    use protobuf::Chars;

    use super::super::testutil::*;
    use super::*;

    #[test]
    fn awaiting_uat_invalid_handshake() {
        let state = default_uat();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_HANDSHAKE,
            ),
            Err(HandshakeError::InvalidHandshake)
        ))
    }

    #[test]
    fn awaiting_uat_already_connected() {
        let state = default_uat();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::PLAYER_ALREADY_CONNECTED,
            ),
            Err(HandshakeError::PlayerAlreadyConnected)
        ))
    }

    #[test]
    fn awaiting_uat_wrong_version() {
        let state = default_uat();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::WRONG_VERSION,
            ),
            Err(HandshakeError::WrongVersion)
        ))
    }

    #[test]
    fn awaiting_uat_userdb_load_failure_or_user_unknown() {
        let state = default_uat();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_PASSWORD,
            ),
            Err(HandshakeError::UserDbUnloadableOrUserUnknown)
        ))
    }

    #[test]
    fn awaiting_uat_player_blocked() {
        let state = default_uat();
        let mut player_blocked_msg = PlayerBlockedMsg::new();
        player_blocked_msg.set_reason(
            Chars::from_bytes(Bytes::from_static(
                PLAYER_BLOCKED_REASON.as_bytes(),
            ))
            .unwrap(),
        );
        assert!(matches!(
            state.handle_player_blocked_msg(player_blocked_msg),
            Err(HandshakeError::PlayerBlocked(msg)) if &*msg == PLAYER_BLOCKED_REASON
        ))
    }

    #[test]
    fn awaiting_uat_invalid_public_key() {
        let state = default_uat();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_PUBLIC_KEY,
            ),
            Err(HandshakeError::PublicKeyNotFound)
        ))
    }

    #[test]
    fn awaiting_uat_unexpected_message() {
        let state = default_uat();
        let mut handshake_msg = HandshakeMsg::new();
        let _connection_successful_msg =
            handshake_msg.mut_connection_successful_msg();
        assert!(matches!(
            state.handle_unexpected_message(handshake_msg),
            Err(HandshakeError::UnexpectedMessage(_))
        ))
    }

    #[test]
    fn awaiting_uat_server_disconnected() {
        let state = default_uat();
        assert!(matches!(
            state.handle_server_disconnect(),
            Err(HandshakeError::ServerDisconnected)
        ))
    }

    #[test]
    fn awaiting_uat_invalid_salt() {
        let state = default_uat();
        let mut uat_msg = default_shared_use_auth_type_msg();
        uat_msg.set_salt(vec![].into());
        assert!(matches!(
            state.handle_use_auth_type_msg(uat_msg),
            Err(HandshakeError::InvalidChallengeSalt)
        ))
    }

    #[test]
    fn awaiting_uat_invalid_iv() {
        let state = default_uat();
        let mut uat_msg = default_shared_use_auth_type_msg();
        uat_msg.set_iv(vec![].into());
        assert!(matches!(
            state.handle_use_auth_type_msg(uat_msg),
            Err(HandshakeError::InvalidChallengeIv)
        ))
    }

    #[test]
    fn awaiting_uat_no_challenges() {
        let state = default_uat();
        let mut uat_msg = default_shared_use_auth_type_msg();
        uat_msg.set_challenge(vec![].into());
        assert!(matches!(
            state.handle_use_auth_type_msg(uat_msg),
            Err(HandshakeError::ServerOfferedNoChallenges)
        ))
    }

    #[test]
    fn awaiting_uat_expecting_password() {
        let state = default_uat();
        let uat_msg = default_shared_use_auth_type_msg();
        assert!(matches!(
            state.handle_use_auth_type_msg(uat_msg),
            Ok(ExpectingCredential::ExpectingPassword(_state))
        ))
    }
}
