//! Pure typestate implementation of the MapTool authentication handshake
//! This interface is intended as an implementation detail to ease testing
//! and building a more dynamic interface on top.
//!
//! Methods take the specific type of handshake message,
//! and other methods exist for terminal states to provide a consistent interface
//! for returning appropriate errors.
//! Errors are only returned when the state is unrecoverable
//! and reauthentication requires tearing down the connection and restarting.
//! Recoverable failures are different states.
//! TODO: build byte queue interface on top.

mod awaiting_approval;
mod awaiting_asymmetric_use_auth_type;
mod awaiting_connection_successful;
mod awaiting_use_auth_type_or_request_public_key;
mod expecting_credential;
mod expecting_password;
mod expecting_rsa_decrypt;
mod handshake_error;
mod init;
#[cfg(test)]
pub(crate) mod testutil;

pub use awaiting_approval::AwaitingApproval;
pub use awaiting_asymmetric_use_auth_type::AwaitingAsymmetricUseAuthType;
pub use awaiting_connection_successful::AwaitingConnectionSuccessful;
pub use awaiting_use_auth_type_or_request_public_key::{
    AwaitingUseAuthTypeOrRequestPublicKey, PinCode,
};
pub use expecting_credential::ExpectingCredential;
pub use expecting_password::{DecryptedChallenge, ExpectingPassword};
pub use expecting_rsa_decrypt::ExpectingRsaDecrypt;
pub use handshake_error::HandshakeError;
pub use init::init;

#[cfg(test)]
mod test {
    use super::testutil::*;
    use super::*;

    use crate::crypto::make_symmetric_cipher;
    use crate::parse_pubkey;
    use crate::proto::handshake::{PublicKeyAddedMsg, RequestPublicKeyMsg};
    use crate::proto::{
        data_transfer_objects::RoleDto, handshake::ConnectionSuccessfulMsg,
    };

    #[test]
    fn shared_password_success() {
        let (state, mut client_init_msg) =
            init(PLAYER_NAME, Some(&PUBLIC_KEY_DER));

        let name = client_init_msg.take_player_name();
        assert_eq!(&*name, PLAYER_NAME);
        // We assume the contents are correct and only use message contents where necessary for the handshake algorithm

        let use_auth_type_msg = default_shared_use_auth_type_msg();

        let state = match state
            .handle_use_auth_type_msg(use_auth_type_msg)
            .expect("Should receive a valid UseAuthTypeMsg")
        {
            ExpectingCredential::ExpectingPassword(state) => state,
            _ => unreachable!("Should only expect password"),
        };

        let (i, plaintext) = state
            .decrypt_challenges(&GM_PASSWORD)
            .expect("GM Password should decrypt a challenge");
        assert_eq!(i, 0, "GM Password should decrypt the first challenge");
        let (state, client_auth_msg) = state.provide_plaintext(plaintext);
        let iv: [u8; 16] = client_auth_msg
            .get_iv()
            .try_into()
            .expect("Should have a 16 byte IV");
        let cipher = make_symmetric_cipher(&SALT, &iv);
        let mut plaintext = cipher
            .decrypt(&GM_PASSWORD, client_auth_msg.get_challenge_response())
            .expect("Decryption should never fail");
        plaintext.reverse();
        assert_eq!(&plaintext, GM_CHALLENGE_NONCE.as_bytes());

        let mut connection_successful_msg = ConnectionSuccessfulMsg::new();
        connection_successful_msg.set_role_dto(RoleDto::GM);
        let _session =
            state.handle_connection_successful_msg(connection_successful_msg);
    }

    #[test]
    fn easyconnect_success() {
        let (state, mut client_init_msg) =
            init(PLAYER_NAME, Some(&PUBLIC_KEY_DER));

        let name = client_init_msg.take_player_name();
        assert_eq!(&*name, PLAYER_NAME);

        let mut request_public_key_msg = RequestPublicKeyMsg::new();
        request_public_key_msg.set_pin(default_pin_chars());

        let (state, pin, mut public_key_upload_msg) = state
            .handle_request_public_key_msg(request_public_key_msg)
            .expect("Pin should be a 4 digit number");
        assert_eq!(pin, default_pin_unsigned());
        let uploaded_public_key_chars = public_key_upload_msg.take_public_key();
        let uploaded_public_key = parse_pubkey(&*uploaded_public_key_chars)
            .expect("Uploaded public key should be valid");
        let mut public_key_added_msg = PublicKeyAddedMsg::new();
        public_key_added_msg
            .set_public_key(public_key_upload_msg.take_public_key());

        let (state, repeat_client_init_msg) = state
            .handle_public_key_added_msg(public_key_added_msg)
            .expect("Added public key should match uploaded key");
        assert_eq!(client_init_msg, repeat_client_init_msg);

        let use_auth_type_msg = default_asymmetric_use_auth_type_msg();
        let (state, challenge) = state
            .handle_use_auth_type_msg(use_auth_type_msg)
            .expect("Should receive a valid asymmetric challenge");

        // TODO: Should the crypto be part of the API?
        // The delineation is that

        let decrypted_challenge =
            todo!("use decrypt {challenge:?} with private key");
        let (state, client_auth_msg) =
            state.provide_decrypted_challenge(decrypted_challenge);
        todo!("validate {client_auth_msg:?}");

        let mut connection_successful_msg = ConnectionSuccessfulMsg::new();
        connection_successful_msg.set_role_dto(RoleDto::GM);
        let _session =
            state.handle_connection_successful_msg(connection_successful_msg);
    }
}
