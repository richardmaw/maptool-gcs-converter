use bytes::Bytes;

use super::{
    awaiting_connection_successful::AwaitingConnectionSuccessful,
    handshake_error::HandshakeError,
};
use crate::{
    crypto::make_symmetric_cipher,
    proto::handshake::{ClientAuthMsg, HandshakeMsg, HandshakeResponseCodeMsg},
};

#[derive(Debug)]
pub struct ExpectingPassword<Name>
where
    Name: AsRef<str>,
{
    pub(crate) name: Name,
    pub(crate) salt: [u8; 16],
    pub(crate) iv: [u8; 16],
    pub(crate) challenges: Vec<Bytes>,
}

// TODO: Find that reference to per-instance type tickets/tokens
// so returned tokens can't be used with the wrong instance.
// IIRC it was some for<'a> stuff.
#[derive(Debug)]
pub struct DecryptedChallenge<'a> {
    password: &'a str,
    plaintext: Box<[u8]>,
}

impl<Name> ExpectingPassword<Name>
where
    Name: AsRef<str>,
{
    pub fn decrypt_challenges<'a>(
        &self,
        password: &'a str,
    ) -> Option<(usize, DecryptedChallenge<'a>)> {
        let cipher = make_symmetric_cipher(&self.salt, &self.iv);

        for (i, ciphertext) in self.challenges.iter().enumerate() {
            let plaintext: Box<[u8]> =
                match cipher.decrypt(password, ciphertext) {
                    Err(_) => continue,
                    Ok(plaintext) => plaintext,
                }
                .into();
            if !plaintext.starts_with(self.name.as_ref().as_bytes()) {
                continue;
            }
            return Some((
                i,
                DecryptedChallenge {
                    password,
                    plaintext,
                },
            ));
        }
        None
    }

    pub fn provide_plaintext(
        self,
        DecryptedChallenge {
            password,
            mut plaintext,
        }: DecryptedChallenge,
    ) -> (AwaitingConnectionSuccessful, ClientAuthMsg) {
        // Guard against programmer error passing a DecryptedChallenge from another instance
        assert!(plaintext.starts_with(self.name.as_ref().as_bytes()));
        let nonce = &mut plaintext[self.name.as_ref().as_bytes().len()..];
        nonce.reverse();
        let iv: [u8; 16] = rand::random();
        let cipher = make_symmetric_cipher(&self.salt, &iv);
        let ciphertext = cipher.encrypt(password, nonce).expect(
            "Encryption should be infallible since parameters are hard-coded",
        );
        let mut client_auth_msg = ClientAuthMsg::new();
        client_auth_msg.set_iv(iv.to_vec().into());
        client_auth_msg.set_challenge_response(ciphertext.into());
        (AwaitingConnectionSuccessful, client_auth_msg)
    }

    pub fn handle_handshake_response_code_msg(
        self,
        handshake_response_code_msg: HandshakeResponseCodeMsg,
    ) -> Result<(), HandshakeError> {
        Err(match handshake_response_code_msg {
            HandshakeResponseCodeMsg::INVALID_HANDSHAKE => {
                HandshakeError::InvalidHandshake
            }
            _ => {
                let mut handshake_msg = HandshakeMsg::new();
                handshake_msg.set_handshake_response_code_msg(
                    handshake_response_code_msg,
                );
                HandshakeError::UnexpectedMessage(handshake_msg)
            }
        })
    }

    pub fn handle_unexpected_message(
        self,
        handshake_msg: HandshakeMsg,
    ) -> Result<(), HandshakeError> {
        Err(HandshakeError::UnexpectedMessage(handshake_msg))
    }

    pub fn handle_server_disconnect(self) -> Result<(), HandshakeError> {
        Err(HandshakeError::ServerDisconnected)
    }
}

#[cfg(test)]
mod test {
    use super::super::testutil::*;
    use super::*;
    use crate::proto::handshake::HandshakeResponseCodeMsg;

    #[test]
    fn expecting_pass_server_disconnected() {
        let state = default_expecting_password();
        assert!(matches!(
            state.handle_server_disconnect(),
            Err(HandshakeError::ServerDisconnected)
        ))
    }

    #[test]
    fn expecting_pass_unexpected_message() {
        let state = default_expecting_password();
        let mut handshake_msg = HandshakeMsg::new();
        let _connection_successful_msg =
            handshake_msg.mut_connection_successful_msg();
        assert!(matches!(
            state.handle_unexpected_message(handshake_msg),
            Err(HandshakeError::UnexpectedMessage(_))
        ))
    }

    #[test]
    fn expecting_pass_invalid_handshake() {
        let state = default_expecting_password();
        assert!(matches!(
            state.handle_handshake_response_code_msg(
                HandshakeResponseCodeMsg::INVALID_HANDSHAKE,
            ),
            Err(HandshakeError::InvalidHandshake)
        ))
    }

    #[test]
    fn expecting_pass_wrong_password() {
        let state = default_expecting_password();
        assert!(matches!(
            state.decrypt_challenges("an incorrect password"),
            None
        ));
    }

    #[test]
    fn expecting_pass_player_password() {
        let state = default_expecting_password();
        let (idx, player_password_response) = state
            .decrypt_challenges(PLAYER_PASSWORD)
            .expect("Player password should be valid");
        assert_eq!(idx, 1);
        let (state, client_auth_msg) =
            state.provide_plaintext(player_password_response);
        let iv: [u8; 16] = client_auth_msg
            .get_iv()
            .try_into()
            .expect("Should have a 16 byte IV");
        let cipher = make_symmetric_cipher(&SALT, &iv);
        let mut plaintext = cipher
            .decrypt(&PLAYER_PASSWORD, client_auth_msg.get_challenge_response())
            .expect("Decryption should never fail");
        plaintext.reverse();
        assert_eq!(&plaintext, PLAYER_CHALLENGE_NONCE.as_bytes());
        assert!(matches!(state, AwaitingConnectionSuccessful))
    }

    #[test]
    fn expecting_pass_gm_password() {
        let state = default_expecting_password();
        let (idx, gm_password_response) = state
            .decrypt_challenges(GM_PASSWORD)
            .expect("GM password should be valid");
        assert_eq!(idx, 0);
        let (state, client_auth_msg) =
            state.provide_plaintext(gm_password_response);
        let iv: [u8; 16] = client_auth_msg
            .get_iv()
            .try_into()
            .expect("Should have a 16 byte IV");
        let cipher = make_symmetric_cipher(&SALT, &iv);
        let mut plaintext = cipher
            .decrypt(&GM_PASSWORD, client_auth_msg.get_challenge_response())
            .expect("Decryption should never fail");
        plaintext.reverse();
        assert_eq!(&plaintext, GM_CHALLENGE_NONCE.as_bytes());
        assert!(matches!(state, AwaitingConnectionSuccessful))
    }
}
