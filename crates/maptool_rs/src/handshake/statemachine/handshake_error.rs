use protobuf::Chars;

use crate::proto::handshake::HandshakeMsg;

/// Unrecoverable handshake errors that require tearing down the connection to retry.
#[derive(Debug)]
pub enum HandshakeError {
    /// The socket or WebRTC datachannel closed unexpectedly
    ServerDisconnected,
    /// An unexpected message type was sent or unexpected variant of message
    UnexpectedMessage(HandshakeMsg),
    /// The server was not expecting a given message
    InvalidHandshake,
    /// There is already a player with the given name connected
    PlayerAlreadyConnected,
    /// The server's version doesn't match the provided
    WrongVersion,
    UserDbUnloadableOrUserUnknown,
    PlayerBlocked(Chars),
    PublicKeyNotFound,
    InvalidChallengeSalt,
    InvalidChallengeIv,
    /// If the server provided no challenges then it's impossible to generate a response
    ServerOfferedNoChallenges,
    UnexpectedUseSharedPasswordAuth,
    /// The provided challenge response was not valid for any password challenges
    InvalidPassword,
    /// The provided challenge response was not valid for the recorded public key
    InvalidPublicKey,
}
