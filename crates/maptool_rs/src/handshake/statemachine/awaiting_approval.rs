use super::{
    awaiting_asymmetric_use_auth_type::AwaitingAsymmetricUseAuthType,
    handshake_error::HandshakeError,
};
use crate::proto::handshake::{ClientInitMsg, PublicKeyAddedMsg};

#[derive(Debug)]
pub struct AwaitingApproval;

impl AwaitingApproval {
    pub fn handle_public_key_added_msg(
        self,
        public_key_added_msg: PublicKeyAddedMsg,
    ) -> Result<(AwaitingAsymmetricUseAuthType, ClientInitMsg), HandshakeError>
    {
        todo!("Use {public_key_added_msg:?}")
    }
}
