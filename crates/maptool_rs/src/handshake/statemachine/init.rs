use bytes::Bytes;
use protobuf::Chars;

use super::awaiting_use_auth_type_or_request_public_key::AwaitingUseAuthTypeOrRequestPublicKey;
use crate::{handshake::md5sum_pubkey, proto::handshake::ClientInitMsg};

// TOOD: Should this have an Init state to wait for connection established?
pub fn init<Name, PublicKey>(
    name: Name,
    public_key: Option<PublicKey>,
) -> (
    AwaitingUseAuthTypeOrRequestPublicKey<Name, PublicKey>,
    ClientInitMsg,
)
where
    Name: AsRef<str>,
    PublicKey: AsRef<[u8]>,
{
    let public_key_md5 = public_key
        .as_ref()
        .map_or_else(Chars::new, |key| md5sum_pubkey(key).into());
    let mut client_init_msg = ClientInitMsg::new();
    client_init_msg.set_player_name(Chars::from(name.as_ref()));
    client_init_msg.set_version(
        Chars::from_bytes(Bytes::from_static(crate::VERSION.as_bytes()))
            .expect("Chars initialized from str should be valid UTF8"),
    );
    client_init_msg.set_public_key_md5(public_key_md5);

    (
        AwaitingUseAuthTypeOrRequestPublicKey { name, public_key },
        client_init_msg,
    )
}

#[cfg(test)]
mod test {
    use super::super::testutil::*;
    use super::*;

    #[test]
    fn init_no_public_key() {
        // Test included for completeness
        let (state, client_init_msg) = init(PLAYER_NAME, None);
        let expected = default_uat();
        assert!(state.name == expected.name);
        assert!(state.public_key == expected.public_key);
        assert_eq!(&*client_init_msg.get_public_key_md5(), "");
    }

    #[test]
    fn init_md5_public_key() {
        // Test included for completeness
        let (_state, client_init_msg) =
            init(PLAYER_NAME, Some(&PUBLIC_KEY_DER));
        assert_eq!(
            &*client_init_msg.get_public_key_md5(),
            PUBLIC_KEY_MAPTOOL_PEM_MD5
        );
    }
}
