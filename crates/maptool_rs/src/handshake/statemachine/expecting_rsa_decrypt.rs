use super::awaiting_connection_successful::AwaitingConnectionSuccessful;
use crate::proto::handshake::ClientAuthMsg;

#[derive(Debug)]
pub struct ExpectingRsaDecrypt;

impl ExpectingRsaDecrypt {
    /// Validate the decrypted challenge and return a challenge response message if correct.
    ///
    /// Since the public key has already been provided it is not possible to try alternative keys
    /// but this permits only decrypting the private key when it's needed
    /// or potentially using a private key held in a TPM.
    pub fn provide_decrypted_challenge(
        self,
        decrypted_challenge: &[u8],
    ) -> (AwaitingConnectionSuccessful, ClientAuthMsg) {
        todo!()
    }
}
