use std::borrow::Cow;

use bytes::Bytes;
use protobuf::Chars;
use rsa::{
    pkcs8::{DecodePrivateKey, DecodePublicKey},
    RsaPrivateKey, RsaPublicKey,
};

use super::{
    awaiting_use_auth_type_or_request_public_key::AwaitingUseAuthTypeOrRequestPublicKey,
    expecting_password::ExpectingPassword,
};
use crate::{
    crypto::{make_asymmetric_encryptor, make_symmetric_cipher},
    proto::handshake::{AuthTypeEnum, UseAuthTypeMsg},
};

pub(crate) const PLAYER_NAME: &'static str = "test";
pub(crate) const TEST_PIN: &'static str = "1234";
pub(crate) const PLAYER_BLOCKED_REASON: &'static str = "Conduct unbecoming";
pub(crate) const GM_PASSWORD: &'static str = "gm password";
pub(crate) const PLAYER_PASSWORD: &'static str = "player password";
pub(crate) const GM_CHALLENGE_NONCE: &'static str = "ABCDEFGHIJKLMNO";
pub(crate) const PLAYER_CHALLENGE_NONCE: &'static str = "abcdefghijklmno";
pub(crate) const SALT: [u8; 16] = [
    0xCA, 0xFE, 0xBE, 0xEF, 0xCA, 0xFE, 0xBE, 0xEF, 0xCA, 0xFE, 0xBE, 0xEF,
    0xCA, 0xFE, 0xBE, 0xEF,
];
pub(crate) const IV: [u8; 16] = [
    0xBE, 0xEF, 0xCA, 0xFE, 0xBE, 0xEF, 0xCA, 0xFE, 0xBE, 0xEF, 0xCA, 0xFE,
    0xBE, 0xEF, 0xCA, 0xFE,
];

pub(crate) const PRIVATE_KEY_DER: &'static [u8] =
    include_bytes!("testpriv.der");
pub(crate) const PUBLIC_KEY_DER: &'static [u8] = include_bytes!("testpub.der");
pub(crate) const PUBLIC_KEY_MAPTOOL_PEM_MD5: &'static str =
    include_str!("testpub.mtpem.md5");

pub(crate) fn load_public_key() -> RsaPublicKey {
    RsaPublicKey::from_public_key_der(PUBLIC_KEY_DER)
        .expect("Should be a valid public key")
}
pub(crate) fn load_private_key() -> RsaPrivateKey {
    RsaPrivateKey::from_pkcs8_der(PRIVATE_KEY_DER)
        .expect("Should be a valid private key")
}

pub(crate) fn default_pin_chars() -> Chars {
    unsafe {
        Chars::from_bytes(Bytes::from_static(TEST_PIN.as_bytes()))
            .unwrap_unchecked()
    }
}
pub(crate) fn default_pin_unsigned() -> u16 {
    unsafe { TEST_PIN.parse().unwrap_unchecked() }
}

pub(crate) fn default_uat(
) -> AwaitingUseAuthTypeOrRequestPublicKey<&'static str, [u8; 0]> {
    AwaitingUseAuthTypeOrRequestPublicKey {
        name: PLAYER_NAME,
        public_key: Option::<[u8; 0]>::None,
    }
}

pub(crate) fn default_challenges() -> (Bytes, Bytes) {
    let cipher = make_symmetric_cipher(&SALT, &IV);
    let mut gm_challenge = PLAYER_NAME.to_string();
    gm_challenge.push_str(GM_CHALLENGE_NONCE);
    let gm_challenge = cipher
        .encrypt(&GM_PASSWORD, gm_challenge.as_ref())
        .expect("Encryption should succeed");
    let mut player_challenge = PLAYER_NAME.to_string();
    player_challenge.push_str(PLAYER_CHALLENGE_NONCE);
    let player_challenge = cipher
        .encrypt(&PLAYER_PASSWORD, player_challenge.as_ref())
        .expect("Encryption should succeed");

    (gm_challenge.into(), player_challenge.into())
}

pub(crate) fn default_shared_use_auth_type_msg() -> UseAuthTypeMsg {
    let mut use_auth_type_msg = UseAuthTypeMsg::new();
    use_auth_type_msg.set_auth_type(AuthTypeEnum::SHARED_PASSWORD);
    use_auth_type_msg.set_salt(Bytes::from_static(&SALT));
    use_auth_type_msg.set_iv(Bytes::from_static(&IV));
    let (gm_challenge, player_challenge) = default_challenges();
    use_auth_type_msg.set_challenge(vec![gm_challenge, player_challenge]);
    use_auth_type_msg
}

pub(crate) fn default_asymmetric_challenge() -> Bytes {
    let public_key = load_public_key();
    let encryptor = make_asymmetric_encryptor(Cow::Borrowed(&public_key));
    let mut challenge = PLAYER_NAME.to_string();
    challenge.push_str(GM_CHALLENGE_NONCE);
    let challenge = encryptor.encrypt(&challenge);

    challenge.into()
}

pub(crate) fn default_asymmetric_use_auth_type_msg() -> UseAuthTypeMsg {
    let mut use_auth_type_msg = UseAuthTypeMsg::new();
    use_auth_type_msg.set_auth_type(AuthTypeEnum::ASYMMETRIC_KEY);
    use_auth_type_msg.set_salt(Bytes::from_static(&SALT));
    use_auth_type_msg.set_iv(Bytes::from_static(&IV));
    let challenge = default_asymmetric_challenge();
    use_auth_type_msg.set_challenge(vec![challenge]);
    use_auth_type_msg
}

pub(crate) fn default_expecting_password() -> ExpectingPassword<&'static str> {
    let (gm_challenge, player_challenge) = default_challenges();
    ExpectingPassword {
        name: PLAYER_NAME,
        salt: SALT.clone(),
        iv: IV.clone(),
        challenges: vec![gm_challenge, player_challenge],
    }
}
