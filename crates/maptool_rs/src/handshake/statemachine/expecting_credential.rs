use bytes::Bytes;

use super::{
    expecting_password::ExpectingPassword,
    expecting_rsa_decrypt::ExpectingRsaDecrypt,
};

#[derive(Debug)]
pub enum ExpectingCredential<Name>
where
    Name: AsRef<str>,
{
    ExpectingPassword(ExpectingPassword<Name>),
    ExpectingRsaDecrypt {
        state: ExpectingRsaDecrypt,
        challenge: Bytes,
    },
}
