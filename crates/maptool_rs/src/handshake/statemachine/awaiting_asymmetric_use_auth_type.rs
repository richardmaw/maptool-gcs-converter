use bytes::Bytes;

use super::{
    expecting_rsa_decrypt::ExpectingRsaDecrypt, handshake_error::HandshakeError,
};
use crate::proto::handshake::{AuthTypeEnum, UseAuthTypeMsg};

#[derive(Debug)]
pub struct AwaitingAsymmetricUseAuthType;
impl AwaitingAsymmetricUseAuthType {
    pub fn handle_use_auth_type_msg(
        self,
        use_auth_type_msg: UseAuthTypeMsg,
    ) -> Result<(ExpectingRsaDecrypt, Bytes), HandshakeError> {
        match use_auth_type_msg.get_auth_type() {
            AuthTypeEnum::ASYMMETRIC_KEY => {
                todo!("Define helper function for handling this")
            }
            AuthTypeEnum::SHARED_PASSWORD => {
                Err(HandshakeError::UnexpectedUseSharedPasswordAuth)
            }
        }
    }
}
