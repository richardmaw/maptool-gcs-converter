use core::future::Future;

use protobuf::Chars;
use thiserror::Error;

use crate::{
    handshake::{
        rpc::{self, InitError, InitState, RespondError, WaitForApprovalError},
        Session,
    },
    proto,
    types::{AsymmetricResponseError, Challenge, SymmetricResponseError},
};

#[derive(Debug, Error)]
#[cfg_attr(feature = "miette", derive(miette::Diagnostic))]
pub enum HandshakeError<ReportPinError, PasswordError, PrivateKeyError>
where
    ReportPinError: std::error::Error + 'static,
    PasswordError: std::error::Error + 'static,
    PrivateKeyError: std::error::Error + 'static,
{
    #[error("Handshake init failed")]
    InitFailed(#[source] InitError),
    #[error("Reporting Easy Connect PIN failed")]
    ReportPinFailed(#[source] ReportPinError),
    #[error("Waiting for approval failed")]
    WaitForApprovalFailed(#[source] WaitForApprovalError),
    #[error("Password was requested and none was provided")]
    PasswordRequested,
    #[error("Get Password function failed")]
    GetPasswordFailed(#[source] PasswordError),
    #[error("Processing symmetric challenge failed")]
    ProcessSymmetricChallengeFailed(#[source] SymmetricResponseError),
    #[error("Get Private Key function failed")]
    GetPrivateKeyFailed(#[source] PrivateKeyError),
    #[error("Processing asymmetric challenge failed")]
    ProcessAsymmetricChallengeFailed(#[source] AsymmetricResponseError),
    #[error("Responding failed")]
    RespondFailed(#[source] RespondError),
}
pub struct KeyOptions<
    ReportPinError,
    ReportPinFunction,
    ReportPinResult,
    PrivateKey,
    PrivateKeyError,
    PrivateKeyFunction,
    PrivateKeyResult,
    PublicKey,
> where
    ReportPinFunction: FnOnce(&str) -> ReportPinResult,
    ReportPinResult: Future<Output = Result<(), ReportPinError>>,
    PrivateKeyFunction: FnOnce() -> PrivateKeyResult,
    PrivateKeyResult: Future<Output = Result<PrivateKey, PrivateKeyError>>,
    PrivateKey: AsRef<[u8]>,
    PublicKey: AsRef<[u8]>,
{
    pub public_key: PublicKey,
    pub report_pin_fn: ReportPinFunction,
    pub private_key_fn: PrivateKeyFunction,
}

// TODO: recoverable errors should return the connection
pub async fn handshake<
    Connection,
    ReportPinError,
    ReportPinResult,
    ReportPinFunction,
    Password,
    PasswordError,
    PasswordFunction,
    PasswordResult,
    PrivateKey,
    PrivateKeyError,
    PrivateKeyResult,
    PrivateKeyFunction,
    PublicKey,
>(
    connection: Connection,
    name: Chars,
    key: Option<
        KeyOptions<
            ReportPinError,
            ReportPinFunction,
            ReportPinResult,
            PrivateKey,
            PrivateKeyError,
            PrivateKeyFunction,
            PrivateKeyResult,
            PublicKey,
        >,
    >,
    password_fn: Option<PasswordFunction>,
) -> Result<
    (
        proto::handshake::ConnectionSuccessfulMsg,
        Session<Connection>,
    ),
    HandshakeError<ReportPinError, PasswordError, PrivateKeyError>,
>
where
    // TODO: This is not a nice API to mock, and isn't the best API for uring.
    // It would be nice to be able to use a channel for local and testing.
    // They have a difference in constraints that make awkward APIs though.
    // let buf = con.send(buf).await?.unwrap_or_default();
    // let buf = con.recv(if con.uses_buf() { Some(buf) } else { None }).await?;
    Connection: futures_io::AsyncRead + futures_io::AsyncWrite + Unpin,
    ReportPinFunction: FnOnce(&str) -> ReportPinResult,
    ReportPinResult: Future<Output = Result<(), ReportPinError>>,
    PrivateKeyFunction: FnOnce() -> PrivateKeyResult,
    PrivateKeyResult: Future<Output = Result<PrivateKey, PrivateKeyError>>,
    PrivateKey: AsRef<[u8]> + Sized,
    ReportPinError: std::error::Error + 'static,
    Password: AsRef<[u8]>,
    PasswordError: std::error::Error + 'static,
    PasswordFunction: FnOnce() -> PasswordResult,
    PasswordResult: Future<Output = Result<Password, PasswordError>>,
    PublicKey: AsRef<[u8]> + Sized,
    PrivateKeyError: std::error::Error + 'static,
{
    use HandshakeError::*;
    let (public_key, report_pin_fn, private_key_fn) = match key {
        None => (None, None, None),
        Some(KeyOptions {
            public_key,
            report_pin_fn,
            private_key_fn,
        }) => (Some(public_key), Some(report_pin_fn), Some(private_key_fn)),
    };
    let init_state = rpc::init(connection, &name, public_key)
        .await
        .map_err(InitFailed)?;
    let (connection, challenge) = match init_state {
        InitState::PendingApproval { connection, pin } => {
            let report_pin_fn = report_pin_fn.expect(
                "report_pin_fn should not be required if key not provided",
            );
            report_pin_fn(&pin).await.map_err(ReportPinFailed)?;
            connection
                .wait_for_approval()
                .await
                .map_err(WaitForApprovalFailed)?
        }
        InitState::PendingResponse {
            connection,
            challenge,
        } => (connection, challenge),
    };
    let (iv, response) = match challenge {
        Challenge::Asymmetric(challenge) => {
            let private_key_fn = private_key_fn.expect(
                "private_key_fn should not be required if key not provided",
            );
            let private_key =
                private_key_fn().await.map_err(GetPrivateKeyFailed)?;
            let response = challenge
                .response(&name, &private_key)
                .map_err(ProcessAsymmetricChallengeFailed)?;
            (None, response)
        }
        Challenge::Symmetric(challenge) => {
            let password_fn = password_fn.ok_or(PasswordRequested)?;
            let password = password_fn().await.map_err(GetPasswordFailed)?;
            let (iv, response) = challenge
                .response(&name, &password)
                .map_err(ProcessSymmetricChallengeFailed)?;
            (Some(iv), response)
        }
    };
    connection
        .respond(iv, response)
        .await
        .map_err(RespondFailed)
}
