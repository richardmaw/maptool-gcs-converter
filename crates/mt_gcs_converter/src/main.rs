mod lib;
mod parse;
mod token;

use std::{
    fs::{self, File, OpenOptions},
    io::{self, BufWriter, ErrorKind, Write},
    path::{self, Path, PathBuf},
    rc::Rc,
    time::SystemTime,
};

use base64::prelude::*;
use clap::{Parser, Subcommand};
use itertools::Itertools;
use jaq_interpret::{Ctx, FilterT, RcIter, Val};
use lazy_static::lazy_static;
use miette::{bail, miette, Context, IntoDiagnostic, NamedSource};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use parse::parse_filter;
use token::Token;

lazy_static! {
    static ref KEY_ID: String = "id".to_owned();
    static ref KEY_NAME: String = "name".to_owned();
    static ref KEY_PORTRAIT: String = "portrait".to_owned();
    static ref KEY_PROPERTIES: String = "properties".to_owned();
    static ref KEY_TOKEN_TYPE: String = "tokenType".to_owned();
}

/// Canonicalize if the path exists, make it absolute if it doesn't,
/// and treat the empty string as the current directory.
// TODO: Canonical newtype so we don't have to worry about the diff_paths expect?
fn force_canonicalize(mut path: &Path) -> miette::Result<Box<Path>> {
    if path == Path::new("") {
        path = Path::new(".");
    }
    match path.canonicalize() {
        Err(e) if e.kind() == ErrorKind::NotFound => path::absolute(path),
        r => r,
    }
    .into_diagnostic()
    .map(Into::into)
    .map_err(Into::into)
}

/// Helper function for optionally prepending a root to a path
fn join_root(root: Option<&Path>, config_dir: &Path, path: &Path) -> PathBuf {
    match root {
        None => config_dir.join(path),
        Some(root) => root.join(path),
    }
}

/// Helper function for optionally stripping a root prefix if the path starts with it
// TODO: Maybe use Canonicalized newtype and unsafe unwrap instead of expect.
fn relative_to(path: &Path, root: &Path) -> Box<Path> {
    pathdiff::diff_paths(path, root).expect("It should be impossible to fail to calculate a relative path to two canonicalized paths").into()
}

#[derive(Parser, Debug)]
struct Roots {
    /// When loading character sheet paths from config files or writing them back
    /// consider them to be relative to this root directory instead of the directory containing the file.
    ///
    /// **NOTE**: This is only for paths inside the config file.
    ///
    /// # Example
    ///
    /// ```bash
    /// $ mt-gcs-converter --character-sheet-root ~/GCS configure -c Skeleton.mgc --character-sheet ~/GCS/Master\ Library/Dungeon\ Fantasy\ RPG/Monsters/Skeleton.gcs ...
    /// $ jq -r .character_sheet Skeleton.mgc
    /// Master Library/Dungeon Fantasy RPG/Monsters/Skeleton.gcs
    /// ```
    #[arg(
        long,
        env = "MGC_CHARACTER_SHEET_ROOT",
        value_parser,
        verbatim_doc_comment
    )]
    character_sheet_root: Option<Box<Path>>,

    /// When loading filter paths from config files or writing them back
    /// consider them to be relative to this root directory instead of the directory containing the file.
    ///
    /// **NOTE**: This is only for paths inside the config file.
    ///
    /// # Example
    ///
    /// ```bash
    /// $ mt-gcs-converter --filter-root ~/src/mt-gurps/filters configure -c Skeleton.mgc --filter ~/src/mt-gurps/filters/corporeal-humanoid-undead.jq ...
    /// $ jq -r .filter Skeleton.mgc
    /// corporeal-humanoid-undead.jq
    /// ```
    #[arg(long, env = "MGC_FILTER_ROOT", value_parser, verbatim_doc_comment)]
    filter_root: Option<Box<Path>>,

    /// When loading token images from config files or writing them back
    /// consider them to be relative to this root directory instead of the directory containing the file.
    ///
    /// **NOTE**: This is only for paths inside the config file.
    ///
    /// # Example
    ///
    /// ```bash
    /// $ mt-gcs-converter --token-image-root ~/Sync/GURPS/tokens configure -c Skeleton.mgc --token-image ~/Sync/GURPS/tokens/skeleton-1.png ...
    /// $ jq -r .token-image Skeleton.mgc
    /// skeleton-1.png
    /// ```
    #[arg(
        long,
        env = "MGC_TOKEN_IMAGE_ROOT",
        value_parser,
        verbatim_doc_comment
    )]
    token_image_root: Option<Box<Path>>,

    /// When loading maptool tokens from config files or writing them back
    /// consider them to be relative to this root directory instead of the directory containing the file.
    ///
    /// **NOTE**: This is only for paths inside the config file.
    ///
    /// # Example
    ///
    /// ```bash
    /// $ mt-gcs-converter --token-root ~/Sync/GURPS/maptool configure -c Skeleton.mgc --token ~/Sync/GURPS/maptool/Skeleton.mgc ...
    /// $ jq -r .token Skeleton.mgc
    /// Skeleton.png
    /// ```
    #[arg(long, env = "MGC_TOKEN_ROOT", value_parser, verbatim_doc_comment)]
    token_root: Option<Box<Path>>,
}

impl Roots {
    fn into_force_canonical(self) -> miette::Result<Self> {
        Ok(Self {
            character_sheet_root: self
                .character_sheet_root
                .as_deref()
                .map(|root| {
                    force_canonicalize(root).with_context(|| {
                        format!(
                            "Failed to canonicalize character sheet root {}",
                            root.display()
                        )
                    })
                })
                .transpose()?,
            filter_root: self
                .filter_root
                .as_deref()
                .map(|root| {
                    force_canonicalize(root).with_context(|| {
                        format!(
                            "Failed to canonicalize filter root {}",
                            root.display()
                        )
                    })
                })
                .transpose()?,
            token_image_root: self
                .token_image_root
                .as_deref()
                .map(|root| {
                    force_canonicalize(root).with_context(|| {
                        format!(
                            "Failed to canonicalize token image root {}",
                            root.display()
                        )
                    })
                })
                .transpose()?,
            token_root: self
                .token_root
                .as_deref()
                .map(|root| {
                    force_canonicalize(root).with_context(|| {
                        format!(
                            "Failed to canonicalize token root {}",
                            root.display()
                        )
                    })
                })
                .transpose()?,
        })
    }
}

// TODO: These paths are currently interpreted as relative to CWD,
//       they should be relative to the config file.
//       Change Config to a versioned enum
#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
struct Config {
    character_sheet: Box<Path>,
    filter: Box<Path>,
    #[serde(skip_serializing_if = "Option::is_none")]
    token_image: Option<Box<Path>>,
    token: Box<Path>,
}

#[derive(Parser, Debug)]
struct ConfigureOptions {
    /// Config for providing parameters not provided by command-line
    /// and any command-line parameters are saved back,
    /// so in subsequent re-runs only the config need be passed.
    /// If not provided defaults to character sheet with .mgc extension.
    #[arg(long, short, value_parser)]
    config: Option<Box<Path>>,

    /// Character sheet to read attributes from
    /// Defaults to value in config, if no value in config an error is raised.
    #[arg(long, short = 's', value_parser)]
    character_sheet: Option<Box<Path>>,

    /// Path to jq filter file that must return an object with properties:
    ///
    /// * *name*: A mandatory token name.
    /// * *portrait*: An optional base64-encoded image to use as the token image if `--token-image` is omitted.
    /// * *properties*: An object whose keys match the campaign's token properties
    ///                 and _should_ contain an *io.gitlab.richardmaw.maptool-gcs-converter.id* key
    ///                 with the id from the character sheet to allow cross-referencing.
    ///
    /// # Example
    ///
    /// ```
    /// {
    ///     name: .profile.name,
    ///     portrait: .profile.portrait,
    ///     tokenType: "Basic",
    ///     properties: .attributes|map(
    ///         if .attr_id == "basic_speed" then {
    ///             bs: .calc.value
    ///         } elif .attr_id == "basic_move" then {
    ///             move: .calc.value
    ///         } elif .calc.current then {
    ///             (.attr_id): .calc.current,
    ///             ("max"+.attr_id): .calc.value
    ///         } else {
    ///             (.attr_id): .calc.value
    ///         } end
    ///     )|add +
    ///     {
    ///         dodge: .calc.dodge[0],
    ///         bl: .calc.basic_lift,
    ///         dmgs: .calc.swing,
    ///         dmgt: .calc.thrust,
    ///         sm: .profile.SM // 0,
    ///         dr: .settings.body_type.locations|map(select(.id == "torso").calc.dr.all)|add/length,
    ///         "io.gitlab.richardmaw.maptool-gcs-converter.id": .id,
    ///     }|with_entries(.value |= tostring)
    /// }
    /// ```
    #[arg(long, short, value_parser, verbatim_doc_comment)]
    filter: Option<Box<Path>>,
    /// Optional token image, used for map token.
    /// If not provided portrait from character sheet is used.
    /// If sheet lacks a portrait then an error is raised.
    #[arg(long, short = 't', value_parser)]
    token_image: Option<Box<Path>>,

    /// Output file, input with .rptok suffix if omitted
    #[arg(long, short = 'o', value_parser)]
    token: Option<Box<Path>>,
}

impl ConfigureOptions {
    fn run(self, roots: Roots) -> miette::Result<()> {
        let roots = roots.into_force_canonical()?;
        let (
            config,
            config_dir,
            config_character_sheet,
            config_filter_path,
            config_token_image_path,
            config_token_path,
        ) = match self.config.as_deref() {
            None => (None, None, None, None, None, None),
            Some(config_path) => {
                let config_dir = config_path
                    .parent()
                    .ok_or_else(|| {
                        miette!(
                        "Config path should have at least 1 non-root component"
                    )
                    })
                    .and_then(|path| {
                        force_canonicalize(&path).with_context(|| {
                            format!(
                                "Failed to canonicalize config directory {}",
                                path.display()
                            )
                        })
                    })?;
                let config: Option<Config> = File::open(config_path)
                    .ok()
                    .and_then(|reader| serde_json::from_reader(reader).ok());
                match config {
                    None => (None, None, None, None, None, None),
                    Some(config) => {
                        let config_character_sheet = join_root(
                            roots.character_sheet_root.as_deref(),
                            &config_dir,
                            &config.character_sheet,
                        )
                        .into();
                        let config_filter_path = join_root(
                            roots.filter_root.as_deref(),
                            &config_dir,
                            &config.filter,
                        )
                        .into();
                        let config_token_image_path: Option<Box<Path>> =
                            config.token_image.as_deref().map(|token_image| {
                                join_root(
                                    roots.token_image_root.as_deref(),
                                    &config_dir,
                                    token_image,
                                )
                                .into()
                            });
                        let config_token_path = join_root(
                            roots.token_root.as_deref(),
                            &config_dir,
                            &config.token,
                        )
                        .into();
                        (
                            Some(config),
                            Some(config_dir),
                            Some(config_character_sheet),
                            Some(config_filter_path),
                            config_token_image_path,
                            Some(config_token_path),
                        )
                    }
                }
            }
        };

        let character_sheet_path: Box<Path> = self
            .character_sheet
            .or(config_character_sheet)
            .ok_or_else(|| {
                miette!("Character sheet must either be provided by command-line or config")
            }).and_then(|path| force_canonicalize(&path).with_context(|| format!("Failed to canonicalize character sheet path {}", path.display())))?;
        let config_path: PathBuf = self.config.map_or_else(
            || {
                let mut config_path: PathBuf =
                    character_sheet_path.clone().into();
                config_path.set_extension("mgc");
                config_path
            },
            Into::into,
        );
        let config_dir = config_dir.map(Ok).unwrap_or_else(|| {
            config_path
                .parent()
                .ok_or_else(|| {
                    miette!(
                        "Config path should have at least 1 non-root component"
                    )
                })
                .and_then(|path| {
                    force_canonicalize(&path).with_context(|| {
                        format!(
                            "Failed to canonicalize config directory {}",
                            path.display()
                        )
                    })
                })
        })?;
        let filter_path: Box<Path> = self
            .filter
            .or(config_filter_path)
            .ok_or_else(|| {
                miette!(
                    "Filter must either be provided by command-line or config"
                )
            })
            .and_then(|path| {
                force_canonicalize(&path).with_context(|| {
                    format!(
                        "Failed to canonicalize filter path {}",
                        path.display()
                    )
                })
            })?;
        let token_image_path = self
            .token_image
            .or(config_token_image_path)
            .map(|path| {
                force_canonicalize(&path).with_context(|| {
                    format!(
                        "Failed to caononicalize token image path {}",
                        path.display()
                    )
                })
            })
            .transpose()?;
        let token_path =
            self.token.or(config_token_path).unwrap_or_else(|| {
                let mut token_path: PathBuf =
                    character_sheet_path.clone().into();
                token_path.set_extension("rptok");
                token_path.into()
            });
        let token_path =
            force_canonicalize(&token_path).with_context(|| {
                format!(
                    "Failed to canonicalize character sheet path {}",
                    token_path.display()
                )
            })?;

        // TODO: Factor this and the above into a constructor that canonicalizes?
        let new_config = Config {
            character_sheet: relative_to(
                &character_sheet_path,
                roots.character_sheet_root.as_deref().unwrap_or(&config_dir),
            ),
            filter: relative_to(
                &filter_path,
                roots.filter_root.as_deref().unwrap_or(&config_dir),
            ),
            token_image: token_image_path.map(|token_image| {
                relative_to(
                    &token_image,
                    roots.token_image_root.as_deref().unwrap_or(&config_dir),
                )
            }),
            token: relative_to(
                &token_path,
                roots.token_root.as_deref().unwrap_or(&config_dir),
            ),
        };
        if config.map_or_else(|| true, |config| config != new_config) {
            serde_json::to_writer(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(config_path)
                    .into_diagnostic()?,
                &new_config,
            )
            .into_diagnostic()?;
        }

        Ok(())
    }
}

#[derive(Parser, Debug)]
struct GenerateOptions {
    /// Path to file to touch when the MapTool token is generated
    ///
    /// Make and Ninja need an output file for target rules to determine
    /// whether build rules need to be re-run.
    /// Ideally this would be the token itself, but since that path is contained inside
    /// the config and we don't want to have to recalculate our build targets every time mgc files
    /// change we can use the well-established pattern of making a stamp file
    /// which acts as a proxy for when the rule was last run.
    #[arg(short, long, value_parser)]
    stamp: Option<Box<Path>>,

    /// Write dependencies of output in Makefile syntax to this file
    ///
    /// When using a build-system we would like to only rebuild when necessary.
    /// Source files that include paths to files they depend on pose a problem
    /// because listing all the dependencies in build files to ensure this
    /// can get out of sync when paths are added to the source files.
    /// It would be unacceptably slow to have the build system re-parse the source file every
    /// build to figure out whether a rebuild is necessary, so the solution is that
    /// as part of a build additional dependency information is collected and can be reused.
    /// This works because the source file's modification time is sufficient for the first build,
    /// subsequent builds that haven't modified the source file have the dependency information
    /// available, and if the source file is modified it doesn't matter that the dependency
    /// information is out of date.
    ///
    /// This is equivalent to gcc's -MM and -MF options.
    ///
    /// The paths are written relative to the directory the depfile is contained within
    /// rather than the provided --*-root options,
    /// and if --stamp is given it is used for the output target instead of the token path.
    #[arg(short, long, value_parser)]
    depfile: Option<Box<Path>>,

    /// Path to token generation config
    #[arg(value_parser)]
    config: Box<Path>,
}

fn write_depfile_escaped(
    mut writer: impl Write,
    path: impl AsRef<[u8]>,
) -> io::Result<()> {
    for c in path.as_ref() {
        match c {
            b' ' => writer.write_all(b"\\ "),
            b'\n' => writer.write_all(b"\\\n"),
            b':' => writer.write_all(b"\\:"),
            b'|' => writer.write_all(b"\\|"),
            b'$' => writer.write_all(b"$$"),
            c => writer.write_all(&[*c]),
        }?
    }
    Ok(())
}

impl GenerateOptions {
    fn run(self, roots: Roots) -> miette::Result<()> {
        let config_reader = File::open(&self.config)
            .into_diagnostic()
            .wrap_err_with(|| {
                format!("Failed to open config file {}", self.config.display())
            })?;
        let config: Config = serde_json::from_reader(config_reader)
            .into_diagnostic()
            .with_context(|| {
                format!("Failed to parse config file {}", self.config.display())
            })?;
        let config_dir = self.config.parent().ok_or_else(|| {
            miette!("Config path should have at least 1 non-root component")
        })?;

        let character_sheet_path = join_root(
            roots.character_sheet_root.as_deref(),
            &config_dir,
            &config.character_sheet,
        );
        let filter_path = join_root(
            roots.filter_root.as_deref(),
            &config_dir,
            &config.filter,
        );
        let token_image_path: Option<Box<Path>> =
            config.token_image.map(|token_image| {
                join_root(
                    roots.token_image_root.as_deref(),
                    &config_dir,
                    &token_image,
                )
                .into()
            });
        let token_path =
            join_root(roots.token_root.as_deref(), &config_dir, &config.token);

        let output_path = self.stamp.as_deref().unwrap_or(&token_path);
        // TODO: Check modification time of input paths and skip generating if the token path's
        // modification time is newer.

        let filter_code: String = fs::read_to_string(&filter_path)
            .into_diagnostic()
            .with_context(|| {
                format!(
                    "Failed to read jq extract filter {}",
                    filter_path.display()
                )
            })?;

        let filter = parse_filter(&filter_code).map_err(|e| {
            e.with_source_code(NamedSource::new(
                filter_path.to_string_lossy(),
                filter_code,
            ))
        })?;

        let input: Value = serde_json::from_reader(
            File::open(&character_sheet_path)
                .into_diagnostic()
                .with_context(|| {
                    format!(
                        "Failed to open character sheet file {}",
                        character_sheet_path.display()
                    )
                })?,
        )
        .into_diagnostic()
        .with_context(|| {
            format!(
                "Failed to parse character sheet file {} as json",
                character_sheet_path.display()
            )
        })?;

        // TODO: factor out and call .with_context(|| format!("Failed to extract values from character sheet {} with filter {}"))
        let (name, token_image, properties, token_type) = {
            let inputs = RcIter::new(core::iter::empty());

            let value = filter
                .run((Ctx::new([], &inputs), Val::from(input)))
                .exactly_one()
                .map_err(|it| match it.collect::<Result<Vec<_>, _>>() {
                    Ok(v) => {
                        miette!("Instead of one result filter matched {:?}", v)
                    }
                    Err(e) => miette!("{}", e),
                })?
                .map_err(|e| miette!("{e:?}"))?;

            let mut map = match value {
                Val::Obj(map) => {
                    Rc::into_inner(map).expect("map has one ref count")
                }
                _ => {
                    bail!("Returned non-object, expected object with name, portrait and properties keys");
                }
            };

            let name = match map
                .remove(&*KEY_NAME)
                .ok_or_else(|| miette!("Returned object lacks name"))?
            {
                Val::Str(s) => (*s).clone(),
                _ => bail!("Returned object name key has non-str value"),
            };

            let portrait: Option<Vec<u8>> = match map
                .remove(&*KEY_PORTRAIT)
                .ok_or_else(|| miette!("Returned object lacks portrait"))?
            {
                Val::Null => None,
                Val::Str(s) => Some(BASE64_STANDARD.decode(s.as_ref()).expect(
                    "Returned object portrait key has non-base64 value",
                )),
                _ => bail!(
                    "Returned object portrait key has non-str or null value"
                ),
            };

            let token_image: Vec<u8> = token_image_path
                .as_deref()
                .map(fs::read)
                .transpose()
                .into_diagnostic()?
                .or(portrait)
                .ok_or_else(|| {
                    miette!(
                        "Config file {} does not include a token image path and character sheet {} does not include a portrait",
                        self.config.display(),
                        character_sheet_path.display(),
                    )
                })?;

            // TODO: Add the filter path and sheet path to error context
            let properties = match map
                .remove(&*KEY_PROPERTIES)
                .ok_or_else(|| miette!("Returned object lacks properties"))?
            {
                Val::Obj(o) => {
                    Rc::into_inner(o).expect("properties has one ref count")
                }
                _ => bail!("Returned object portrait key has non-object value"),
            };

            // TODO: Add the filter path and sheet path to error context
            let token_type = match map
                .remove(&*KEY_TOKEN_TYPE)
                .ok_or_else(|| miette!("Returned object lacks token type"))?
            {
                Val::Str(s) => (*s).clone(),
                _ => bail!("Returned object name key has non-str value"),
            };
            (name, token_image, properties, token_type)
        };
        // TODO: Maybe load old tokens and check if properties and art differ

        let mut token = Token::new(name);
        for (k, v) in properties {
            token
                .set_property(k.as_ref(), Some(v))
                .map_err(|e| miette!("Should have a valid value: {:?}", e))?;
        }
        token.set_token_type(token_type).into_diagnostic()?;
        token
            .set_token_image(token_image.as_ref())
            .into_diagnostic()?;

        // TODO: write to temporary file so we build state is consistent if depfile generation
        // fails.
        token
            .into_zipfile(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(&token_path)
                    .into_diagnostic()?,
            )
            .into_diagnostic()
            .with_context(|| {
                format!("Failed to write zipfile to {}", token_path.display())
            })?;

        // Generate the depfile
        if let Some(depfile) = self.depfile {
            let depfile_dir = depfile.parent().ok_or_else(|| {
                miette!(
                    "depfile path {} should have at least 1 non-root component",
                    depfile.display()
                )
            })?;
            let depfile_dir =
                force_canonicalize(depfile_dir).with_context(|| {
                    format!(
                        "Failed to canonicalize depfile directory {}",
                        depfile_dir.display()
                    )
                })?;
            let output_path = relative_to(
                &force_canonicalize(&output_path).with_context(|| {
                    format!(
                        "Failed to canonicalize output path {}",
                        output_path.display()
                    )
                })?,
                &depfile_dir,
            );
            let character_sheet_path = relative_to(
                &force_canonicalize(&character_sheet_path).with_context(
                    || {
                        format!(
                            "Failed to canonicalize character sheet path {}",
                            character_sheet_path.display()
                        )
                    },
                )?,
                &depfile_dir,
            );
            let filter_path = relative_to(
                &force_canonicalize(&filter_path).with_context(|| {
                    format!(
                        "Failed to canonicalize filter path {}",
                        filter_path.display()
                    )
                })?,
                &depfile_dir,
            );
            let token_image_path = token_image_path
                .map(|path| {
                    force_canonicalize(&path)
                        .with_context(|| {
                            format!(
                                "Failed to canonicalize filter path {}",
                                filter_path.display()
                            )
                        })
                        .map(|path| relative_to(&path, &depfile_dir))
                })
                .transpose()?;

            // TODO: Spool in a tempfile
            let mut depfile = BufWriter::new(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(&depfile)
                    .into_diagnostic()?,
            );
            write_depfile_escaped(
                &mut depfile,
                output_path.as_os_str().as_encoded_bytes(),
            )
            .into_diagnostic()?;
            depfile.write_all(b": ").into_diagnostic()?;
            write_depfile_escaped(
                &mut depfile,
                character_sheet_path.as_os_str().as_encoded_bytes(),
            )
            .into_diagnostic()?;
            depfile.write_all(b" ").into_diagnostic()?;
            write_depfile_escaped(
                &mut depfile,
                filter_path.as_os_str().as_encoded_bytes(),
            )
            .into_diagnostic()?;
            if let Some(token_image_path) = token_image_path {
                depfile.write_all(b" ").into_diagnostic()?;
                write_depfile_escaped(
                    &mut depfile,
                    token_image_path.as_os_str().as_encoded_bytes(),
                )
                .into_diagnostic()?;
            }
            depfile.write_all(b"\n").into_diagnostic()?;
            depfile.flush().into_diagnostic()?;
        }

        // Update the stamp file
        if let Some(stamp_path) = self.stamp {
            let stamp_file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(&stamp_path)
                .into_diagnostic()
                .with_context(|| {
                    format!(
                        "Failed to open stamp file {}",
                        stamp_path.display()
                    )
                })?;
            stamp_file
                .set_modified(SystemTime::now())
                .into_diagnostic()
                .with_context(|| {
                    format!(
                        "Failed to update modification time of stamp file {}",
                        stamp_path.display()
                    )
                })?;
        }

        Ok(())
    }
}

#[derive(Subcommand, Debug)]
#[command(author, version, about, long_about = None)]
enum Command {
    Configure(ConfigureOptions),
    Generate(GenerateOptions),
}

impl Command {
    fn run(self, roots: Roots) -> miette::Result<()> {
        match self {
            Command::Configure(configure_options) => {
                configure_options.run(roots)
            }
            Command::Generate(generate_options) => generate_options.run(roots),
        }
    }
}

#[derive(Parser, Debug)]
struct Args {
    #[command(flatten)]
    roots: Roots,
    #[command(subcommand)]
    subcommand: Command,
}

impl Args {
    fn run(self) -> miette::Result<()> {
        self.subcommand.run(self.roots)
    }
}

fn main() -> miette::Result<()> {
    Args::parse().run()
}
