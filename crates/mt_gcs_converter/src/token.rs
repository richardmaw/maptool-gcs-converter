use std::borrow::Cow;
use std::collections::HashMap;
use std::io::{Cursor, Error as IoError, Read, Seek, Write};
use std::rc::{Rc, Weak};

use indexmap::IndexMap;

use crate::lib::image::{
    error::ImageError, io::Reader as ImageReader, ImageFormat,
};
use crate::lib::md5;
use crate::lib::uuid::Uuid;
use crate::lib::zip::{
    result::ZipError, write::FileOptions, CompressionMethod, ZipWriter,
};
use crate::token::data::value::MD5Key;
use crate::token::data::{
    Asset, AssetType, Content, Properties, Value, ASSET_ROOT, CONTENT_ROOT,
    PROPERTIES_ROOT,
};

mod data;

const LARGE_THUMBNAIL_SIZE: u32 = 500;
const SMALL_THUMBNAIL_SIZE: u32 = 50;

pub(crate) struct Token<'a> {
    name: Cow<'a, str>,
    properties: IndexMap<Cow<'a, str>, Option<Value<'a>>>,
    // md5 hashed content-addressed map
    assets: HashMap<md5::Digest, (ImageFormat, Weak<[u8]>)>,
    token_image: Option<(md5::Digest, ImageFormat, Rc<[u8]>)>,
    token_type: Cow<'a, str>,
    macros: Vec<(Cow<'a, str>, Cow<'a, str>)>,
}
#[derive(Debug, thiserror::Error)]
pub enum SetPropertyError<E> {
    #[error("Value is not valid")]
    ValueNotValid(#[from] E),
}

#[derive(Debug, thiserror::Error)]
pub enum IntoZipfileError {
    #[error("Start Asset Metadata")]
    StartAssetMetadata(ZipError),
    #[error("Write Asset Metadata")]
    FormatAssetMetadata(quick_xml::DeError),
    #[error("Write Asset Metadata")]
    WriteAssetMetadata(IoError),
    #[error("Start Asset Data")]
    StartAssetData(ZipError),
    #[error("Write Asset Data")]
    WriteAssetData(IoError),
    #[error("Decode token image")]
    DecodeTokenImage(ImageError),
    #[error("Start large thumbnail")]
    StartLargeThumbnail(ZipError),
    #[error("Encode large thumbnail")]
    EncodeLargeThumbnail(ImageError),
    #[error("Write large thumbnail")]
    WriteLargeThumbnail(IoError),
    #[error("Start small thumbnail")]
    StartSmallThumbnail(ZipError),
    #[error("Encode small thumbnail")]
    EncodeSmallThumbnail(ImageError),
    #[error("Write small thumbnail")]
    WriteSmallThumbnail(IoError),
    #[error("Start Content Metadata")]
    StartContentMetadata(ZipError),
    #[error("Write Content Metadata")]
    FormatContentMetadata(quick_xml::DeError),
    #[error("Write Content Metadata")]
    WriteContentMetadata(IoError),
    #[error("Start Properties Metadata")]
    StartPropertiesMetadata(ZipError),
    #[error("Write Properties Metadata")]
    FormatPropertiesMetadata(quick_xml::DeError),
    #[error("Write Properties Metadata")]
    WritePropertiesMetadata(IoError),
}

impl<'a> Token<'a> {
    pub(crate) fn new(name: impl Into<Cow<'a, str>>) -> Self {
        Self {
            name: name.into(),
            properties: Default::default(),
            assets: Default::default(),
            token_image: Default::default(),
            token_type: Default::default(),
            macros: Default::default(),
        }
    }
    pub(crate) fn set_property<V>(
        &mut self,
        key: impl AsRef<str>,
        value: Option<V>,
    ) -> Result<(), SetPropertyError<V::Error>>
    where
        V: TryInto<Value<'a>>,
    {
        let value = value.map(TryInto::try_into).transpose()?;
        if let Some(v) = self.properties.get_mut(key.as_ref()) {
            *v = value;
            return Ok(());
        }
        let key = key.as_ref().to_string().into();
        self.properties.insert(key, value);
        Ok(())
    }
    /// Validate that the image has a recognisable format then record it for later
    pub(crate) fn set_token_image(
        &mut self,
        bytes: impl Into<Box<[u8]>>,
    ) -> Result<(), ImageError> {
        let bytes = bytes.into();
        let digest = md5::compute(&bytes);
        let format = image::guess_format(&bytes)?;
        let bytes = Rc::from(bytes);
        self.assets
            .insert(digest.clone(), (format.clone(), Rc::downgrade(&bytes)));
        self.token_image = Some((digest, format, bytes));
        Ok(())
    }
    pub(crate) fn set_token_type(
        &mut self,
        token_type: impl Into<Cow<'a, str>>,
    ) -> Result<(), ImageError> {
        self.token_type = token_type.into();
        Ok(())
    }
    /// Add a macro to the token, they will be emitted in order with macro index starting at 1.
    pub(crate) fn add_macro(
        &mut self,
        label: impl Into<Cow<'a, str>>,
        r#macro: impl Into<Cow<'a, str>>,
    ) {
        self.macros.push((label.into(), r#macro.into()))
    }
    pub(crate) fn into_zipfile<W>(self, w: W) -> Result<(), IntoZipfileError>
    where
        W: Read + Write + Seek,
    {
        use IntoZipfileError::*;
        let mut zip_writer = ZipWriter::new(w);
        let name = self.name.as_ref().to_owned();
        for (hash, (format, bytes)) in self.assets {
            let bytes = match bytes.upgrade() {
                Some(bytes) => bytes,
                None => continue,
            };
            let extension = format.extensions_str()[0];
            zip_writer
                .start_file(
                    format!("assets/{:032x}", u128::from_be_bytes(hash.0)),
                    FileOptions::default()
                        .compression_method(CompressionMethod::Stored),
                )
                .map_err(StartAssetMetadata)?;
            let id: MD5Key<'a> = hash.into();
            let asset =
                Asset::new(id, name.clone(), extension, AssetType::Image);
            // TODO: quick_xml sets a bound on fmt::Write rather than io::Write
            // which means zip writer needs an intermediary String.
            let s = quick_xml::se::to_string_with_root(ASSET_ROOT, &asset)
                .map_err(FormatAssetMetadata)?;
            zip_writer
                .write_all(s.as_bytes())
                .map_err(WriteAssetMetadata)?;
            zip_writer
                .start_file(
                    format!(
                        "assets/{:032x}.{extension}",
                        u128::from_be_bytes(hash.0)
                    ),
                    FileOptions::default()
                        .compression_method(CompressionMethod::Stored),
                )
                .map_err(StartAssetData)?;
            zip_writer.write_all(&bytes).map_err(WriteAssetData)?;
        }

        let mut content = Content::new(&Uuid::new_v4(), &Uuid::new_v4());
        content.set_name(self.name);
        content.set_property_type(self.token_type);
        content.set_macros(
            self.macros
                .into_iter()
                .map(|(label, command)| (Uuid::new_v4(), label, command)),
        );

        if let Some((hash, format, bytes)) = self.token_image {
            content.set_token_image(hash);

            let reader = ImageReader::with_format(Cursor::new(bytes), format);
            let image = reader.decode().map_err(DecodeTokenImage)?;

            let thumbnail_large = image.thumbnail(
                image.width().min(LARGE_THUMBNAIL_SIZE),
                image.height().min(LARGE_THUMBNAIL_SIZE),
            );
            zip_writer
                .start_file(
                    "thumbnail_large",
                    FileOptions::default()
                        .compression_method(CompressionMethod::Stored),
                )
                .map_err(StartLargeThumbnail)?;
            let mut v = Cursor::new(Vec::new());
            thumbnail_large
                .write_to(&mut v, ImageFormat::Png)
                .map_err(EncodeLargeThumbnail)?;
            std::io::copy(&mut v, &mut zip_writer)
                .map_err(WriteLargeThumbnail)?;

            let thumbnail_small = image.thumbnail(
                image.width().min(SMALL_THUMBNAIL_SIZE),
                image.height().min(SMALL_THUMBNAIL_SIZE),
            );
            zip_writer
                .start_file(
                    "thumbnail",
                    FileOptions::default()
                        .compression_method(CompressionMethod::Stored),
                )
                .map_err(StartSmallThumbnail)?;
            let mut v = Cursor::new(Vec::new());
            thumbnail_small
                .write_to(&mut v, ImageFormat::Png)
                .map_err(EncodeSmallThumbnail)?;
            std::io::copy(&mut v, &mut zip_writer)
                .map_err(WriteSmallThumbnail)?;
        }
        content.set_properties(self.properties);

        zip_writer
            .start_file(
                "content.xml",
                FileOptions::default()
                    .compression_method(CompressionMethod::Deflated),
            )
            .map_err(StartContentMetadata)?;
        let s = quick_xml::se::to_string_with_root(CONTENT_ROOT, &content)
            .map_err(FormatContentMetadata)?;
        zip_writer
            .write_all(s.as_bytes())
            .map_err(WriteContentMetadata)?;

        let properties: Properties = [
            ("version", Value::from("1.13.1")),
            ("herolab", false.into()),
        ]
        .into();
        zip_writer
            .start_file(
                "properties.xml",
                FileOptions::default()
                    .compression_method(CompressionMethod::Stored),
            )
            .map_err(StartPropertiesMetadata)?;
        let s =
            quick_xml::se::to_string_with_root(PROPERTIES_ROOT, &properties)
                .map_err(FormatPropertiesMetadata)?;
        zip_writer
            .write_all(s.as_bytes())
            .map_err(WritePropertiesMetadata)?;

        Ok(())
    }
}
