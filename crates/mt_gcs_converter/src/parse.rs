use jaq_interpret::{Filter, ParseCtx};
use jaq_syn::{
    lex::{span, Expect, Token},
    Lexer,
};
use miette::{Diagnostic, SourceSpan};
use thiserror::Error;

#[derive(Diagnostic, Debug, Error)]
#[diagnostic()]
pub enum LexError {
    #[error("Unexpected end of input")]
    UnexpectedEndOfInput {
        #[label]
        source_span: SourceSpan,
    },
    #[error("Unexpected character")]
    UnexpectedCharacter {
        #[label]
        source_span: SourceSpan,
    },
    #[error("Unclosed delimiter")]
    UnclosedDelimiter {
        #[label]
        source_span: SourceSpan,
    },
}

#[derive(Diagnostic, Debug, Error)]
#[error("Expected {expected}")]
#[diagnostic()]
pub struct ExpectedCharacter {
    expected: &'static str,
    #[related]
    errors: Vec<LexError>,
}

#[derive(Diagnostic, Debug, Error)]
#[error("Tokenising failed")]
#[diagnostic()]
pub struct LexFailed {
    #[related]
    errors: Vec<ExpectedCharacter>,
}

fn lex(filter: &str) -> Result<Vec<Token<&'_ str>>, LexFailed> {
    let tokens = Lexer::new(filter).lex().map_err(|e| {
        let errors = e
            .into_iter()
            .map(|(expected, found)| {
                let c = &found[..found
                    .char_indices()
                    .nth(1)
                    .map_or(found.len(), |(i, _)| i)];
                let source_span = span(filter, c).into();
                let mut errors = Vec::with_capacity(2);
                errors.push(match c {
                    "" => LexError::UnexpectedEndOfInput { source_span },
                    _ => LexError::UnexpectedCharacter { source_span },
                });
                if let Expect::Delim(open) = expected {
                    let source_span = span(filter, open).into();
                    errors.push(LexError::UnclosedDelimiter { source_span });
                }
                ExpectedCharacter {
                    expected: expected.as_str(),
                    errors,
                }
            })
            .collect::<Vec<_>>();
        LexFailed { errors }
    })?;
    Ok(tokens)
}

#[derive(Diagnostic, Debug, Error)]
#[diagnostic()]
pub enum ParseError {
    #[error("Unexpected end of input")]
    UnexpectedEndOfInput {
        #[label]
        source_span: SourceSpan,
    },
    #[error("Unexpected token")]
    UnexpectedToken {
        #[label]
        source_span: SourceSpan,
    },
}

#[derive(Diagnostic, Debug, Error)]
#[error("Expected token {expected}")]
#[diagnostic()]
pub struct ExpectedToken {
    expected: String,
    #[related]
    errors: Vec<ParseError>,
}

#[derive(Diagnostic, Debug, Error)]
#[error("Parsing failed")]
#[diagnostic()]
pub struct ParseFailed {
    #[related]
    errors: Vec<ExpectedToken>,
}

fn parse_tokens(
    filter: &str,
    tokens: &[Token<&'_ str>],
) -> Result<jaq_syn::Main, ParseFailed> {
    let main = jaq_syn::Parser::new(tokens)
        .parse(|parser| {
            // Parse the lexed tokens as a module containing a single term
            parser.module(|parser| parser.term())
        })
        .map_err(|e| {
            let errors = e
                .into_iter()
                .map(|(expected, found)| {
                    let source_span =
                        span(filter, Token::opt_as_str(found, filter)).into();
                    let error = match found {
                        Some(_token) => {
                            ParseError::UnexpectedToken { source_span }
                        }
                        None => {
                            ParseError::UnexpectedEndOfInput { source_span }
                        }
                    };
                    ExpectedToken {
                        expected: expected.as_str().to_owned(),
                        errors: vec![error],
                    }
                })
                .collect::<Vec<_>>();
            ParseFailed { errors }
        })?;
    Ok(main.conv(filter))
}

#[derive(Diagnostic, Debug, Error)]
#[error("{message}")]
#[diagnostic()]
pub struct CompileError {
    message: String,
    #[label]
    source_span: SourceSpan,
}

#[derive(Diagnostic, Debug, Error)]
#[error("Compile failed")]
#[diagnostic()]
pub struct CompileFailed {
    #[related]
    errors: Vec<CompileError>,
}

fn compile(main: jaq_syn::Main) -> Result<Filter, CompileFailed> {
    let mut defs = ParseCtx::new(Vec::new());
    defs.insert_natives(jaq_core::core());
    defs.insert_defs(jaq_std::std());
    let filter = defs.compile(main);
    if !defs.errs.is_empty() {
        let errors = defs
            .errs
            .into_iter()
            .map(|(description, range)| {
                let message = description.to_string();
                let source_span = range.into();
                CompileError {
                    message,
                    source_span,
                }
            })
            .collect();
        return Err(CompileFailed { errors });
    }
    Ok(filter)
}

// NOTE: Result intentionally type-erased because users aren't expected to be able to handle errors differently.
pub(crate) fn parse_filter(filter: &str) -> miette::Result<Filter> {
    let tokens = lex(filter)?;
    let main = parse_tokens(filter, &tokens)?;
    let filter = compile(main)?;
    Ok(filter)
}
