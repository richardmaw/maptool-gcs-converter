use std::borrow::Cow;

use serde::{Deserialize, Serialize};

use super::value::MD5Key;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub(crate) enum AssetType {
    Image,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename = "net.rptools.maptool.model.Asset")]
pub(crate) struct Asset<'a> {
    id: MD5Key<'a>,
    name: Cow<'a, str>,
    extension: Cow<'a, str>,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    r#type: AssetType,
}
impl<'a> Asset<'a> {
    pub(crate) fn new(
        id: impl Into<MD5Key<'a>>,
        name: impl Into<Cow<'a, str>>,
        extension: impl Into<Cow<'a, str>>,
        r#type: AssetType,
    ) -> Self {
        Self {
            id: id.into(),
            name: name.into(),
            extension: extension.into(),
            r#type: r#type,
        }
    }
}
pub(crate) const ASSET_ROOT: &'static str = "net.rptools.maptool.model.Asset";

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_asset() {
        let a = Asset::new(
            "0b21e8af0c7e51d2876cef4cd79da722",
            "Hero",
            "png",
            AssetType::Image,
        );
        assert_eq!(
            quick_xml::se::to_string_with_root(ASSET_ROOT, &a).unwrap(),
            "\
                <net.rptools.maptool.model.Asset>\
                  <id>\
                    <id>0b21e8af0c7e51d2876cef4cd79da722</id>\
                  </id>\
                  <name>Hero</name>\
                  <extension>png</extension>\
                  <type>IMAGE</type>\
                </net.rptools.maptool.model.Asset>\
            "
        )
    }
}
