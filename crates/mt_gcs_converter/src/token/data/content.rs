use std::borrow::Cow;

use crate::lib::uuid::Uuid;
use base64::prelude::*;
use serde::{Deserialize, Serialize};

use super::value::{
    GUIDValue, HotKey, IntValue, MD5KeyValue, Map, StringValue, Value, GUID,
};

#[derive(Clone, Deserialize, Serialize)]
struct Scale(f64);
impl Default for Scale {
    fn default() -> Self {
        Self(1.0)
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
struct OwnerList<'a> {
    #[serde(rename = "$value")]
    contents: Vec<Cow<'a, str>>,
}
impl<'a, IntoIt, E> From<IntoIt> for OwnerList<'a>
where
    IntoIt: IntoIterator<Item = E>,
    E: Into<Cow<'a, str>>,
{
    fn from(it: IntoIt) -> Self {
        Self {
            contents: it.into_iter().map(Into::into).collect(),
        }
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
enum TokenShape {
    #[default]
    Circle,
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
enum TokenType {
    #[default]
    Pc,
    Npc,
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
enum MapLayer {
    #[default]
    Token,
    Hidden,
    Object,
    Background,
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
enum TerrainModifierOperation {
    #[default]
    None,
}

#[derive(Clone, Deserialize, Serialize)]
pub(in super::super) struct PropertyType<'a>(Cow<'a, str>);
impl<'a> Default for PropertyType<'a> {
    fn default() -> Self {
        Self("Basic".into())
    }
}
impl<'a, V> From<V> for PropertyType<'a>
where
    V: Into<Cow<'a, str>>,
{
    fn from(value: V) -> Self {
        Self(value.into())
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct TerrainModifiersIgnored {
    #[serde(
        rename = "net.rptools.maptool.model.Token_-TerrainModifierOperation",
        with = "quick_xml::serde_helpers::text_content"
    )]
    contents: Vec<TerrainModifierOperation>,
}
impl<IntoIt> From<IntoIt> for TerrainModifiersIgnored
where
    IntoIt: IntoIterator<Item = TerrainModifierOperation>,
{
    fn from(it: IntoIt) -> Self {
        Self {
            contents: it.into_iter().collect(),
        }
    }
}
impl Default for TerrainModifiersIgnored {
    fn default() -> Self {
        [TerrainModifierOperation::None].into()
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct LightSourceList;

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct SightType<'a>(Cow<'a, str>);
impl<'a, V> From<V> for SightType<'a>
where
    V: Into<Cow<'a, str>>,
{
    fn from(value: V) -> Self {
        Self(value.into())
    }
}
impl<'a> Default for SightType<'a> {
    fn default() -> Self {
        "Normal".into()
    }
}
#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct NotesType<'a>(Cow<'a, str>);
impl<'a, V> From<V> for NotesType<'a>
where
    V: Into<Cow<'a, str>>,
{
    fn from(value: V) -> Self {
        Self(value.into())
    }
}
impl<'a> Default for NotesType<'a> {
    fn default() -> Self {
        "text/plain".into()
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct State;

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct OuterClass<'a> {
    #[serde(rename = "@reference")]
    reference: Cow<'a, str>,
}
impl<'a> Default for OuterClass<'a> {
    fn default() -> Self {
        Self {
            reference: "../../../..".into(),
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct TaggedValue<'a> {
    #[serde(rename = "@class")]
    class: Cow<'static, str>,
    #[serde(rename = "$text")]
    value: Cow<'a, str>,
}
impl<'a> From<Value<'a>> for TaggedValue<'a> {
    fn from(value: Value<'a>) -> Self {
        use Value::*;
        let class = match value {
            Null => "null",
            Boolean(_) => "boolean",
            String(_) => "string",
            MD5Key(_) => "net.rptools.lib.MD5Key",
            GUID(_) => "net.rptools.maptool.model.GUID",
        }
        .into();
        Self {
            class,
            value: value.to_string().into(),
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
enum KeyValue<'a> {
    #[serde(rename = "net.rptools.CaseInsensitiveHashMap_-KeyValue")]
    KeyValue {
        key: Cow<'a, str>,
        #[serde(skip_serializing_if = "Option::is_none")]
        value: Option<TaggedValue<'a>>,
        #[serde(rename = "outer-class")]
        outer_class: OuterClass<'a>,
    },
}
impl<'a> Default for KeyValue<'a> {
    fn default() -> Self {
        Self::KeyValue {
            key: Default::default(),
            value: Default::default(),
            outer_class: Default::default(),
        }
    }
}
impl<'a> KeyValue<'a> {
    fn new(
        key: impl Into<Cow<'a, str>>,
        value: Option<impl Into<Value<'a>>>,
    ) -> Self {
        Self::KeyValue {
            key: key.into(),
            value: value.map(|v| v.into().into()),
            outer_class: Default::default(),
        }
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct PropertyMapCI<'a> {
    store: Map<StringValue<'a>, KeyValue<'a>>,
}
impl<'a, IntoIt, K, V> From<IntoIt> for PropertyMapCI<'a>
where
    IntoIt: IntoIterator<Item = (K, Option<V>)>,
    K: Into<Cow<'a, str>>,
    V: Into<Value<'a>>,
{
    fn from(it: IntoIt) -> Self {
        Self {
            store: it
                .into_iter()
                .map(|(k, v)| {
                    let k: Cow<'a, str> = k.into();
                    let lower_k = if k.as_ref().chars().all(char::is_lowercase)
                    {
                        k.clone()
                    } else {
                        k.as_ref().to_lowercase().into()
                    };
                    (StringValue::from(lower_k), KeyValue::new(k, v))
                })
                .into(),
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct SpeechMap;

#[derive(Clone, Default, Deserialize, Serialize)]
enum SaveLocation {
    #[default]
    Token,
}

#[derive(Clone, Deserialize, Serialize)]
struct ColorKey<'a>(Cow<'a, str>);
impl<'a> Default for ColorKey<'a> {
    fn default() -> Self {
        Self("default".into())
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct FontSize<'a>(Cow<'a, str>);
impl<'a> Default for FontSize<'a> {
    fn default() -> Self {
        Self("1.00em".into())
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct AutoExecute(bool);
impl Default for AutoExecute {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct AllowPlayerEdits(bool);
impl Default for AllowPlayerEdits {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct DisplayHotKey(bool);
impl Default for DisplayHotKey {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareGroup(bool);
impl Default for CompareGroup {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareSortPrefix(bool);
impl Default for CompareSortPrefix {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareCommand(bool);
impl Default for CompareCommand {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareIncludeLabel(bool);
impl Default for CompareIncludeLabel {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareAutoExecute(bool);
impl Default for CompareAutoExecute {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct CompareApplyToSelectedTokens(bool);
impl Default for CompareApplyToSelectedTokens {
    fn default() -> Self {
        Self(true)
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct MacroButtonProperties<'a> {
    #[serde(rename = "macroUUID")]
    macro_uuid: Uuid,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    save_location: SaveLocation,
    index: i64,
    color_key: ColorKey<'a>,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    hot_key: HotKey,
    command: Cow<'a, str>,
    label: Cow<'a, str>,
    group: Cow<'a, str>,
    #[serde(rename = "sortby")]
    sort_by: Cow<'a, str>,
    auto_execute: AutoExecute,
    include_label: bool,
    apply_to_tokens: bool,
    font_color_key: ColorKey<'a>,
    font_size: FontSize<'a>,
    min_width: Cow<'a, str>,
    max_width: Cow<'a, str>,
    allow_player_edits: AllowPlayerEdits,
    tool_tip: Cow<'a, str>,
    display_hot_key: DisplayHotKey,
    common_macro: bool,
    compare_group: CompareGroup,
    compare_sort_prefix: CompareSortPrefix,
    compare_command: CompareCommand,
    compare_include_label: CompareIncludeLabel,
    compare_auto_execute: CompareAutoExecute,
    compare_apply_to_selected_tokens: CompareApplyToSelectedTokens,
}
impl<'a> MacroButtonProperties<'a> {
    fn new(
        uuid: Uuid,
        index: i64,
        label: impl Into<Cow<'a, str>>,
        command: impl Into<Cow<'a, str>>,
    ) -> Self {
        Self {
            macro_uuid: uuid,
            index: index,
            command: command.into(),
            label: label.into(),
            ..Default::default()
        }
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) enum MacroButtonPropertiesValue<'a> {
    #[serde(rename = "net.rptools.maptool.model.MacroButtonProperties")]
    MacroButtonProperties(MacroButtonProperties<'a>),
}
impl<'a> Default for MacroButtonPropertiesValue<'a> {
    fn default() -> Self {
        Self::MacroButtonProperties(Default::default())
    }
}
impl<'a> MacroButtonPropertiesValue<'a> {
    fn new(
        uuid: Uuid,
        index: i64,
        label: impl Into<Cow<'a, str>>,
        command: impl Into<Cow<'a, str>>,
    ) -> Self {
        Self::MacroButtonProperties(MacroButtonProperties::new(
            uuid, index, label, command,
        ))
    }
}

#[derive(Clone, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Content<'a> {
    /// Per-token unique ID
    id: GUID<'a>,
    being_impersonated: bool,
    /// Per-token unique ID, represents token's vision
    #[serde(rename = "exposedAreaGUID")]
    exposed_area_guid: GUID<'a>,
    /// Token image map, null key is token image, value is md5 hash of image in assets
    image_asset_map: Map<Value<'a>, MD5KeyValue<'a>>,
    x: i64,
    y: i64,
    z: i64,
    anchor_x: i64,
    anchor_y: i64,
    size_scale: Scale,
    last_x: i64,
    last_y: i64,
    snap_to_scale: bool,
    width: i64,
    height: i64,
    iso_width: i64,
    iso_height: i64,
    scale_x: Scale,
    scale_y: Scale,
    /// Mapping of Grid type to token size GUID
    size_map: Map<StringValue<'a>, GUIDValue<'a>>,
    snap_to_grid: bool,
    is_visible: bool,
    visible_only_to_owner: bool,
    vbl_color_sensitivity: i64,
    always_visible_tolerance: i64,
    is_always_visible: bool,
    name: Cow<'a, str>,
    owner_list: OwnerList<'a>,
    owner_type: i64,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    token_shape: TokenShape,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    token_type: TokenType,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    layer: MapLayer,
    property_type: PropertyType<'a>,
    token_opacity: f64,
    speech_name: Cow<'a, str>,
    terrain_modifier: f64,
    #[serde(with = "quick_xml::serde_helpers::text_content")]
    terrain_modifier_operation: TerrainModifierOperation,
    terrain_modifiers_ignored: TerrainModifiersIgnored,
    is_flipped_x: bool,
    is_flipped_y: bool,
    is_flipped_iso: bool,
    light_source_list: LightSourceList,
    sight_type: SightType<'a>,
    has_sight: bool,
    has_image_table: bool,
    notes: Cow<'a, str>,
    notes_type: NotesType<'a>,
    gm_notes: Cow<'a, str>,
    gm_notes_type: NotesType<'a>,
    gm_name: Cow<'a, str>,
    state: State,
    #[serde(rename = "propertyMapCI")]
    property_map_ci: PropertyMapCI<'a>,
    macro_properties_map: Map<IntValue, MacroButtonPropertiesValue<'a>>,
    speech_map: SpeechMap,
    #[serde(rename = "allowURIAccess")]
    allow_uri_access: bool,
}

pub(crate) const CONTENT_ROOT: &'static str = "net.rptools.maptool.model.Token";

impl<'a> Content<'a> {
    pub(in super::super) fn new(id: &Uuid, exposed_area_guid: &Uuid) -> Self {
        let id_buf = BASE64_STANDARD.encode(id.as_bytes());
        let exposed_area_guid_buf =
            BASE64_STANDARD.encode(exposed_area_guid.as_bytes());
        Self {
            id: id_buf.into(),
            being_impersonated: false,
            exposed_area_guid: exposed_area_guid_buf.into(),
            image_asset_map: Default::default(),
            x: 0,
            y: 0,
            z: 0,
            anchor_x: 0,
            anchor_y: 0,
            size_scale: Scale(1.0),
            last_x: 0,
            last_y: 0,
            snap_to_scale: true,
            width: 200,
            height: 200,
            iso_width: 0,
            iso_height: 0,
            scale_x: Scale(1.0),
            scale_y: Scale(1.0),
            size_map: [(
                "net.rptools.maptool.model.HexGridVertical",
                "fwABAQllXDgBAAAAOAABAQ==",
            )]
            .into(),
            snap_to_grid: true,
            is_visible: true,
            visible_only_to_owner: false,
            vbl_color_sensitivity: -1,
            always_visible_tolerance: 2,
            is_always_visible: false,
            name: "".into(),
            owner_list: Default::default(),
            owner_type: 0,
            token_shape: TokenShape::Circle,
            token_type: TokenType::Pc,
            layer: MapLayer::Token,
            property_type: "".into(),
            token_opacity: 1.0,
            speech_name: "".into(),
            terrain_modifier: 0.0,
            terrain_modifier_operation: TerrainModifierOperation::None,
            terrain_modifiers_ignored: Default::default(),
            is_flipped_x: false,
            is_flipped_y: false,
            is_flipped_iso: false,
            light_source_list: LightSourceList,
            sight_type: Default::default(),
            has_sight: true,
            has_image_table: false,
            notes: "".into(),
            notes_type: Default::default(),
            gm_notes: "".into(),
            gm_notes_type: Default::default(),
            gm_name: "".into(),
            state: State,
            property_map_ci: Default::default(),
            macro_properties_map: Default::default(),
            speech_map: SpeechMap,
            allow_uri_access: false,
        }
    }
    /// Sets the Unique token ID and resets any opaque but unique internal IDs
    fn set_id(&mut self, id: Uuid) {
        let s = BASE64_STANDARD.encode(id.as_bytes());
        self.id = s.into();
        let exposed_area_guid = Uuid::new_v4();
        let exposed_area_guid_buf =
            BASE64_STANDARD.encode(exposed_area_guid.as_bytes());
        self.exposed_area_guid = exposed_area_guid_buf.into();
    }
    pub(in super::super) fn set_token_image(&mut self, value: md5::Digest) {
        let value = u128::from_be_bytes(value.0);
        self.image_asset_map = [((), format!("{value:032x}"))].into();
    }
    pub(in super::super) fn set_position(&mut self, x: i64, y: i64, z: i64) {
        self.x = x;
        self.y = y;
        self.z = z;
    }
    pub(in super::super) fn set_name(&mut self, name: impl Into<Cow<'a, str>>) {
        self.name = name.into();
    }
    pub(in super::super) fn set_gm_name(
        &mut self,
        gm_name: impl Into<Cow<'a, str>>,
    ) {
        self.gm_name = gm_name.into();
    }
    pub(in super::super) fn set_speech_name(
        &mut self,
        speech_name: impl Into<Cow<'a, str>>,
    ) {
        self.speech_name = speech_name.into();
    }
    pub(in super::super) fn set_property_type(
        &mut self,
        property_type: impl Into<PropertyType<'a>>,
    ) {
        self.property_type = property_type.into();
    }
    pub(in super::super) fn set_properties<IntoIt, K, V>(&mut self, it: IntoIt)
    where
        IntoIt: IntoIterator<Item = (K, Option<V>)>,
        K: Into<Cow<'a, str>>,
        V: Into<Value<'a>>,
    {
        self.property_map_ci = it.into();
    }
    pub(in super::super) fn set_macros<IntoIt, L, M>(&mut self, it: IntoIt)
    where
        IntoIt: IntoIterator<Item = (Uuid, L, M)>,
        L: Into<Cow<'a, str>>,
        M: Into<Cow<'a, str>>,
    {
        self.macro_properties_map = it
            .into_iter()
            .enumerate()
            .map(|(i, (uuid, label, content))| {
                let index = i64::try_from(i + 1).unwrap();
                let macro_properties = MacroButtonPropertiesValue::new(
                    uuid, index, label, content,
                );
                (index, macro_properties)
            })
            .into();
    }
}

#[cfg(test)]
mod test {
    use uuid::uuid;

    use super::*;
    #[test]
    fn test_content() {
        let mut c = Content::new(
            &uuid!("27dcd9fb-4479-4c6d-a5af-a01a4f4e5da6"),
            &uuid!("027a66fe-c424-4b9d-83c0-bd58ac1c6cf9"),
        );
        c.set_token_image(md5::Digest(
            0x0b21e8af0c7e51d2876cef4cd79da722u128.to_be_bytes(),
        ));
        c.set_position(115, 475, 1);
        c.set_name("Token Name");
        c.set_gm_name("GM Name");
        c.set_speech_name("Speech Bubble Name");
        c.set_property_type("Basic");
        c.set_properties([
            ("ST", Some("10")),
            ("website", None),
            ("Move", Some("5")),
            ("Dodge", Some("8")),
            ("Will", Some("10")),
            ("IQ", Some("10")),
            ("HP", Some("10")),
            ("FP", Some("10")),
            ("BL", Some("50")),
            ("HT", Some("10")),
            ("DR", Some("0")),
            ("BS", Some("5")),
            ("DX", Some("10")),
            ("DmgT", Some("1d-1")),
            ("DmgS", Some("1d")),
            ("SM", Some("0")),
            ("Per", Some("10")),
        ]);
        c.set_macros([
            (uuid!("dccfb380-b00b-43ba-adaf-78ab8f401c0c"),
                "Show Character Sheet",
                "[frame5(\"Shotgun Mike.gcs\"),code: {\
                     [r:getMacroCommand(json.get(getMacroIndexes(\"characterSheetHtml\", \"json\"), 0))]\
                 }]"),
            (uuid!("85bc4845-5b01-4898-b419-df3ae4730751"), "characterSheetHtml", "<body></body>"),
        ]);
        assert_eq!(
            quick_xml::se::to_string_with_root(CONTENT_ROOT, &c).unwrap(),
            "\
                <net.rptools.maptool.model.Token>\
                    <id>\
                        <baGUID>J9zZ+0R5TG2lr6AaT05dpg==</baGUID>\
                    </id>\
                    <beingImpersonated>false</beingImpersonated>\
                    <exposedAreaGUID>\
                        <baGUID>Anpm/sQkS52DwL1YrBxs+Q==</baGUID>\
                    </exposedAreaGUID>\
                    <imageAssetMap>\
                        <entry>\
                            <null/>\
                            <net.rptools.lib.MD5Key>\
                                <id>0b21e8af0c7e51d2876cef4cd79da722</id>\
                            </net.rptools.lib.MD5Key>\
                        </entry>\
                    </imageAssetMap>\
                  <x>115</x>\
                  <y>475</y>\
                  <z>1</z>\
                  <anchorX>0</anchorX>\
                  <anchorY>0</anchorY>\
                  <sizeScale>1</sizeScale>\
                  <lastX>0</lastX>\
                  <lastY>0</lastY>\
                  <snapToScale>true</snapToScale>\
                  <width>200</width>\
                  <height>200</height>\
                  <isoWidth>0</isoWidth>\
                  <isoHeight>0</isoHeight>\
                  <scaleX>1</scaleX>\
                  <scaleY>1</scaleY>\
                  <sizeMap>\
                    <entry>\
                      <string>net.rptools.maptool.model.HexGridVertical</string>\
                      <net.rptools.maptool.model.GUID>\
                        <baGUID>fwABAQllXDgBAAAAOAABAQ==</baGUID>\
                      </net.rptools.maptool.model.GUID>\
                    </entry>\
                  </sizeMap>\
                  <snapToGrid>true</snapToGrid>\
                  <isVisible>true</isVisible>\
                  <visibleOnlyToOwner>false</visibleOnlyToOwner>\
                  <vblColorSensitivity>-1</vblColorSensitivity>\
                  <alwaysVisibleTolerance>2</alwaysVisibleTolerance>\
                  <isAlwaysVisible>false</isAlwaysVisible>\
                  <name>Token Name</name>\
                  <ownerList/>\
                  <ownerType>0</ownerType>\
                  <tokenShape>CIRCLE</tokenShape>\
                  <tokenType>PC</tokenType>\
                  <layer>TOKEN</layer>\
                  <propertyType>Basic</propertyType>\
                  <tokenOpacity>1</tokenOpacity>\
                  <speechName>Speech Bubble Name</speechName>\
                  <terrainModifier>0</terrainModifier>\
                  <terrainModifierOperation>NONE</terrainModifierOperation>\
                  <terrainModifiersIgnored>\
                    <net.rptools.maptool.model.Token_-TerrainModifierOperation>NONE</net.rptools.maptool.model.Token_-TerrainModifierOperation>\
                  </terrainModifiersIgnored>\
                  <isFlippedX>false</isFlippedX>\
                  <isFlippedY>false</isFlippedY>\
                  <isFlippedIso>false</isFlippedIso>\
                  <lightSourceList/>\
                  <sightType>Normal</sightType>\
                  <hasSight>true</hasSight>\
                  <hasImageTable>false</hasImageTable>\
                  <notes/>\
                  <notesType>text/plain</notesType>\
                  <gmNotes/>\
                  <gmNotesType>text/plain</gmNotesType>\
                  <gmName>GM Name</gmName>\
                  <state/>\
                  <propertyMapCI>\
                    <store>\
                      <entry>\
                        <string>st</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>ST</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>website</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>website</key>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>move</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>Move</key>\
                          <value class=\"string\">5</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>dodge</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>Dodge</key>\
                          <value class=\"string\">8</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>will</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>Will</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>iq</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>IQ</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>hp</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>HP</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>fp</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>FP</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>bl</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>BL</key>\
                          <value class=\"string\">50</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>ht</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>HT</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>dr</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>DR</key>\
                          <value class=\"string\">0</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>bs</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>BS</key>\
                          <value class=\"string\">5</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>dx</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>DX</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>dmgt</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>DmgT</key>\
                          <value class=\"string\">1d-1</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>dmgs</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>DmgS</key>\
                          <value class=\"string\">1d</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>sm</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>SM</key>\
                          <value class=\"string\">0</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                      <entry>\
                        <string>per</string>\
                        <net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                          <key>Per</key>\
                          <value class=\"string\">10</value>\
                          <outer-class reference=\"../../../..\"/>\
                        </net.rptools.CaseInsensitiveHashMap_-KeyValue>\
                      </entry>\
                    </store>\
                  </propertyMapCI>\
                  <macroPropertiesMap>\
                    <entry>\
                      <int>1</int>\
                      <net.rptools.maptool.model.MacroButtonProperties>\
                        <macroUUID>dccfb380-b00b-43ba-adaf-78ab8f401c0c</macroUUID>\
                        <saveLocation>Token</saveLocation>\
                        <index>1</index>\
                        <colorKey>default</colorKey>\
                        <hotKey>None</hotKey>\
                        <command>[frame5(&quot;Shotgun Mike.gcs&quot;),code: {\
                            [r:getMacroCommand(json.get(getMacroIndexes(&quot;characterSheetHtml&quot;, &quot;json&quot;), 0))]\
                        }]\
                        </command>\
                        <label>Show Character Sheet</label>\
                        <group/>\
                        <sortby/>\
                        <autoExecute>true</autoExecute>\
                        <includeLabel>false</includeLabel>\
                        <applyToTokens>false</applyToTokens>\
                        <fontColorKey>default</fontColorKey>\
                        <fontSize>1.00em</fontSize>\
                        <minWidth/>\
                        <maxWidth/>\
                        <allowPlayerEdits>true</allowPlayerEdits>\
                        <toolTip/>\
                        <displayHotKey>true</displayHotKey>\
                        <commonMacro>false</commonMacro>\
                        <compareGroup>true</compareGroup>\
                        <compareSortPrefix>true</compareSortPrefix>\
                        <compareCommand>true</compareCommand>\
                        <compareIncludeLabel>true</compareIncludeLabel>\
                        <compareAutoExecute>true</compareAutoExecute>\
                        <compareApplyToSelectedTokens>true</compareApplyToSelectedTokens>\
                      </net.rptools.maptool.model.MacroButtonProperties>\
                    </entry>\
                    <entry>\
                      <int>2</int>\
                      <net.rptools.maptool.model.MacroButtonProperties>\
                        <macroUUID>85bc4845-5b01-4898-b419-df3ae4730751</macroUUID>\
                        <saveLocation>Token</saveLocation>\
                        <index>2</index>\
                        <colorKey>default</colorKey>\
                        <hotKey>None</hotKey>\
                        <command>&lt;body&gt;&lt;/body&gt;</command>\
                        <label>characterSheetHtml</label>\
                        <group/>\
                        <sortby/>\
                        <autoExecute>true</autoExecute>\
                        <includeLabel>false</includeLabel>\
                        <applyToTokens>false</applyToTokens>\
                        <fontColorKey>default</fontColorKey>\
                        <fontSize>1.00em</fontSize>\
                        <minWidth/>\
                        <maxWidth/>\
                        <allowPlayerEdits>true</allowPlayerEdits>\
                        <toolTip/>\
                        <displayHotKey>true</displayHotKey>\
                        <commonMacro>false</commonMacro>\
                        <compareGroup>true</compareGroup>\
                        <compareSortPrefix>true</compareSortPrefix>\
                        <compareCommand>true</compareCommand>\
                        <compareIncludeLabel>true</compareIncludeLabel>\
                        <compareAutoExecute>true</compareAutoExecute>\
                        <compareApplyToSelectedTokens>true</compareApplyToSelectedTokens>\
                      </net.rptools.maptool.model.MacroButtonProperties>\
                    </entry>\
                  </macroPropertiesMap>\
                  <speechMap/>\
                  <allowURIAccess>false</allowURIAccess>\
                </net.rptools.maptool.model.Token>\
            "
        );
    }
}
