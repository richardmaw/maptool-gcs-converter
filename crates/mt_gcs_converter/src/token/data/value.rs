use std::{borrow::Cow, fmt::Display};

use serde::{Deserialize, Serialize};

use crate::lib::serde_json;

#[derive(
    Clone, Debug, Default, Deserialize, Hash, Eq, PartialEq, Serialize,
)]
#[serde(rename_all = "camelCase")]
pub(crate) struct MD5Key<'a> {
    id: Cow<'a, str>,
}
impl<'a> From<Cow<'a, str>> for MD5Key<'a> {
    fn from(value: Cow<'a, str>) -> Self {
        Self { id: value }
    }
}
impl From<String> for MD5Key<'static> {
    fn from(value: String) -> Self {
        Self { id: value.into() }
    }
}
impl<'a> From<&'a str> for MD5Key<'a> {
    fn from(value: &'a str) -> Self {
        Self { id: value.into() }
    }
}
impl From<md5::Digest> for MD5Key<'static> {
    fn from(value: md5::Digest) -> Self {
        Self {
            id: format!("{:032x}", u128::from_be_bytes(value.0)).into(),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Hash, Eq, PartialEq, Serialize)]
pub(crate) struct GUID<'a> {
    #[serde(rename = "baGUID")]
    ba_guid: Cow<'a, str>,
}
impl<'a, E> From<E> for GUID<'a>
where
    E: Into<Cow<'a, str>>,
{
    fn from(value: E) -> Self {
        Self {
            ba_guid: value.into(),
        }
    }
}

#[derive(
    Clone, Debug, Default, Deserialize, Hash, Eq, PartialEq, Serialize,
)]
#[serde(rename_all = "camelCase")]
pub(crate) enum Value<'a> {
    #[default]
    Null,
    Boolean(bool),
    String(Cow<'a, str>),
    #[serde(rename = "net.rptools.lib.MD5Key")]
    MD5Key(MD5Key<'a>),
    #[serde(rename = "net.rptools.maptool.model.GUID")]
    GUID(GUID<'a>),
}
impl<'a> Display for Value<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Value::*;
        match self {
            Null => write!(f, "null"),
            Boolean(b) => write!(f, "{b}"),
            String(s) => write!(f, "{s}"),
            MD5Key(md5key) => write!(f, "{}", md5key.id),
            GUID(guid) => write!(f, "{}", guid.ba_guid),
        }
    }
}
#[cfg(feature = "serde-json")]
#[derive(Debug, thiserror::Error)]
pub(crate) enum ValueFromJsonError {
    #[error("Unconvertable variant")]
    UnconvertableVariant,
}
#[cfg(feature = "serde-json")]
impl<'a> TryFrom<serde_json::Value> for Value<'a> {
    type Error = ValueFromJsonError;
    fn try_from(value: serde_json::Value) -> Result<Self, Self::Error> {
        use serde_json::Value as JsonValue;
        match value {
            JsonValue::Null => Ok(Self::Null),
            JsonValue::Bool(bool) => Ok(Self::Boolean(bool)),
            JsonValue::String(string) => Ok(Self::String(string.into())),
            _ => Err(Self::Error::UnconvertableVariant),
        }
    }
}
#[cfg(all(feature = "jaq-core", feature = "serde-json"))]
impl<'a> TryFrom<jaq_interpret::Val> for Value<'a> {
    type Error = ValueFromJsonError;
    fn try_from(value: jaq_interpret::Val) -> Result<Self, Self::Error> {
        serde_json::Value::from(value).try_into()
    }
}

impl<'a> From<()> for Value<'a> {
    fn from(_: ()) -> Self {
        Self::Null
    }
}

impl<'a> From<bool> for Value<'a> {
    fn from(value: bool) -> Self {
        Self::Boolean(value)
    }
}

impl<'a> From<&'a str> for Value<'a> {
    fn from(value: &'a str) -> Self {
        Self::String(value.into())
    }
}
impl<'a> From<String> for Value<'a> {
    fn from(value: String) -> Self {
        Self::String(value.into())
    }
}
impl<'a> From<Cow<'a, str>> for Value<'a> {
    fn from(value: Cow<'a, str>) -> Self {
        Self::String(value)
    }
}

impl<'a> From<MD5Key<'a>> for Value<'a> {
    fn from(value: MD5Key<'a>) -> Self {
        Self::MD5Key(value)
    }
}

impl<'a> From<GUID<'a>> for Value<'a> {
    fn from(value: GUID<'a>) -> Self {
        Self::GUID(value)
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) enum IntValue {
    Int(i64),
}
impl<'a> Default for IntValue {
    fn default() -> Self {
        Self::Int(Default::default())
    }
}
impl<'a, E> From<E> for IntValue
where
    E: Into<i64>,
{
    fn from(value: E) -> Self {
        Self::Int(value.into())
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) enum StringValue<'a> {
    String(Cow<'a, str>),
}
impl<'a> Default for StringValue<'a> {
    fn default() -> Self {
        Self::String(Default::default())
    }
}
impl<'a, E> From<E> for StringValue<'a>
where
    E: Into<Cow<'a, str>>,
{
    fn from(value: E) -> Self {
        Self::String(value.into())
    }
}

#[derive(Clone, Debug, Deserialize, Hash, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) enum GUIDValue<'a> {
    #[serde(rename = "net.rptools.maptool.model.GUID")]
    GUID(GUID<'a>),
}
impl<'a, E> From<E> for GUIDValue<'a>
where
    E: Into<GUID<'a>>,
{
    fn from(value: E) -> Self {
        Self::GUID(value.into())
    }
}

#[derive(Clone, Debug, Deserialize, Hash, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) enum MD5KeyValue<'a> {
    #[serde(rename = "net.rptools.lib.MD5Key")]
    MD5Key(MD5Key<'a>),
}
impl<'a, V> From<V> for MD5KeyValue<'a>
where
    V: Into<MD5Key<'a>>,
{
    fn from(value: V) -> Self {
        Self::MD5Key(value.into())
    }
}
impl<'a> Default for MD5KeyValue<'a> {
    fn default() -> Self {
        Self::MD5Key(Default::default())
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Entry<K, V> {
    #[serde(rename = "$value")]
    contents: (K, V),
}
impl<K, V> Entry<K, V> {
    pub(crate) fn new(key: impl Into<K>, value: impl Into<V>) -> Self {
        Self {
            contents: (key.into(), value.into()),
        }
    }
}
impl<KIn, VIn, KOut, VOut> From<(KIn, VIn)> for Entry<KOut, VOut>
where
    KIn: Into<KOut>,
    VIn: Into<VOut>,
{
    fn from(value: (KIn, VIn)) -> Self {
        Entry::new(value.0, value.1)
    }
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Map<K, V> {
    #[serde(rename = "entry")]
    map: Vec<Entry<K, V>>,
}
impl<IntoIt, E, K, V> From<IntoIt> for Map<K, V>
where
    IntoIt: IntoIterator<Item = E>,
    E: Into<Entry<K, V>>,
{
    fn from(it: IntoIt) -> Self {
        Self {
            map: it.into_iter().map(Into::into).collect(),
        }
    }
}

#[derive(Clone, Default, Deserialize, Serialize)]
pub(crate) enum HotKey {
    #[default]
    None,
}
