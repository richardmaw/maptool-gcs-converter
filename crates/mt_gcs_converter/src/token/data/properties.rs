use super::value::{Map, StringValue, Value};

pub(crate) type Properties<'a> = Map<StringValue<'a>, Value<'a>>;
pub(crate) const PROPERTIES_ROOT: &'static str = "map";

#[cfg(test)]
mod test {
    use super::{super::value::Entry, *};
    #[test]
    fn test_properties() {
        let m: Properties = [
            ("version", Value::from("1.13.1")),
            ("herolab", false.into()),
        ]
        .into();
        assert_eq!(
            quick_xml::se::to_string_with_root(PROPERTIES_ROOT, &m).unwrap(),
            "\
                <map>\
                    <entry>\
                        <string>version</string>\
                        <string>1.13.1</string>\
                    </entry>\
                    <entry>\
                        <string>herolab</string>\
                        <boolean>false</boolean>\
                    </entry>\
                </map>\
            "
        );
    }
}
