# MapTool GCS Converter

Work in progress project to convert between [GCS][] character sheets
and [MapTool][] tokens.

## Usage

```shell=bash
$ target/debug/mt-gcs-converter shotgun-mike.gcs
$ zipinfo shotgun-mike.rptok
Archive:  shotgun-mike.rptok
Zip file size: 57922 bytes, number of entries: 6
-rw-r--r--  4.6 unx      187 b- stor 23-Oct-16 22:53 assets/f13c0db550003cbdf7a86dadef033e74
-rw-r--r--  4.6 unx    49952 b- stor 23-Oct-16 22:53 assets/f13c0db550003cbdf7a86dadef033e74.webp
-rw-r--r--  4.6 unx        0 b- stor 23-Oct-16 22:53 thumbnail_large
-rw-r--r--  4.6 unx        0 b- stor 23-Oct-16 22:53 thumbnail
-rw-r--r--  4.6 unx     6905 b- stor 23-Oct-16 22:53 content.xml
-rw-r--r--  4.6 unx      136 b- stor 23-Oct-16 22:53 properties.xml
6 files, 57180 bytes uncompressed, 57180 bytes compressed:  0.0%
```

[jaq][] (like JQ but rust) queries are used to extract information
like the character name, portrait and properties.
This is run as a single query that returns a single object like:

```json
{
    "id": "f71b3b84-e712-438f-83cd-b6253849beb6",
    "name": "Skeleton Mike",
    "portrait": "...",
    "properties": {
        "st": "15",
        "hp": "15",
        "maxhp": "15",
        ...
        "sm": "0",
        "dr": "6"
    }
}
```

The name is used for the name of the token.
The portrait is used for the token's image.
The keys for the properties should match the names of the properties
for the token.

It is necessary to run as a single query because [jaq][]'s API
doesn't allow you to run multiple filters over the input
without a lot of spurious repetition and clones.

The standard filter looks something like:

```jq
select(.type == "character")|{
    id: .id,
    name: .profile.name,
    portrait: .profile.portrait,
    properties: ([
        .attributes[]|
        if .attr_id == "basic_speed" then [
            {key: "bs", value: .calc.value}
        ] elif .attr_id == "basic_move" then [
            {key: "move", value: .calc.value}
        ] elif .calc.current then [
            {key: .attr_id, value: .calc.current},
            {key: "max"+.attr_id, value: .calc.value}
        ] else [
            {key: .attr_id, value: .calc.value}
        ] end
    ]|flatten(1)|from_entries)
    + {
        dodge: .calc.dodge[0],
        bl: .calc.basic_lift,
        dmgs: .calc.swing,
        dmgt: .calc.thrust,
        sm: .profile.SM // 0,
        dr: .settings.body_type.locations[]|select(.id == "torso")|.calc.dr.all,
    }|with_entries(.value |= tostring)
}
```

When complete, `mt-gcs-converter` will allow customising the filter
because [GCS][] allows custom attributes
and [MapTool][] campaigns may have tokens with different properties.

The intended API is
`mt-gcs-converter set-extract-filter 'Skeleton Mike.gcs' extract-filter.jq`
which will save the filter in the Third Party data section of the sheet.

The character sheet must have a portrait, as this is used for the token image.
If completed to the point of translating tokens back to character sheets,
then the token's portrait will be saved into the portrait field
and the token and handout will be saved to the Third Party data section.

If possible a copy of the character sheet will be saved into a property
accessible to macros, but the property list is campaign dependant.

Saving values back into character sheets is difficult to support.
If possible it will be limited to point pools like HP/FP
and possibly item uses.

The intended interface for this is providing another filter
where the properties are available as named arguments
and the filter may update the character sheet.

For example, this filter should write the HP back.
```jq
.attributes[]|=select(.attr_id == "hp") |= (
    .calc.current = ($hp|tonumber)
    |.damage = .calc.value - ($hp|tonumber)
)
```

[GCS]: https://gurpscharactersheet.com/
[MapTool]: https://github.com/RPTools/maptool
[jaq]: https://github.com/01mf02/jaq
